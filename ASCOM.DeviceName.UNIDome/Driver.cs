/// <summary>
/// Description:	ASCOM Dome driver for UNIDome
/// Implements:	    ASCOM Dome interface
/// Author:		    Maeva MANUEL maeva.manuel@ogs-technologies.com
/// Date:			19-05-2021
/// Version:        3.0 Created from ASCOM driver template
/// </summary>

#define Dome

using ASCOM;
using ASCOM.Astrometry;
using ASCOM.Astrometry.AstroUtils;
using ASCOM.DeviceInterface;
using ASCOM.Utilities;

using System;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;

namespace ASCOM.UNIDome
{
    /// <summary>
    /// ASCOM Dome Driver for UNIDome
    /// </summary>
    /// <remarks>
    /// Driver's DeviceID is ASCOM.UNIDome.Dome
    /// The Guid attribute sets the CLSID for ASCOM.UNIDome.Dome
    /// The ClassInterface/None attribute prevents an empty interface called
    /// UNIDome from being created and used as the [default] interface
    /// </remarks>
    [Guid("3ea556be-f1aa-4954-a231-7829ba2e91bf")]
    [ClassInterface(ClassInterfaceType.None)]
    public class Dome : IDomeV2
    {
        /// <value>ASCOM DeviceID (COM ProgID) for this driver</value>
        internal static string driverID = "ASCOM.UNIDome.Dome";
        /// <value>driver description that displays in ASCOM Chooser</value>
        private static string driverDescription = "ASCOM Dome Driver for UNIDome";
        /// <value>comPortProfileName used for profil persistence</value>
        internal static string comPortProfileName = "COM Port";
        /// <value>comPortDefault by default to COM11 virtual, COM3 real</value>
        internal static string comPortDefault = "COM3";
        /// <value>comPortDefaultalt by default to COM10 virtual, COM6 real</value>
        internal static string comPortDefaultalt = "COM6";
        /// <value>traceStateProfileName used for profil persistence</value>
        internal static string traceStateProfileName = "Trace Level";
        /// <value>traceStateDefault by default to false</value>
        internal static string traceStateDefault = "false";
        /// <value>variable to hold the COM port</value>
        internal static string comPort;
        /// <value>variable to hold the trace logger object (log file)</value>
        internal TraceLogger tl;
        /// <value>variable to hold the connected state</value>
        private bool connectedState;
        /// <value>variable to hold an ASCOM Utilities object</value>
        private Util utilities;

        /// <value>SerialConnection is the DomeSerial communication</value>
        private DomeSerial SerialConnection;
        /// <value>unidome_form is the UNIDome GUI</value>
        private UNIDomeForm unidome_form;
        /// <value>Config object to access private state machine flags</value>
        private static Config Config;
        /// <value>dome_gui is a thread waiting for user input/output in the GUI</value>
        public Thread dome_gui;
        /// <value>tracking_on_thread is a thread waiting for click on TRACKING ON button</value>
        public Thread tracking_on_thread;
        /// <value>stop_dome_thread is a thread waiting for click on STOP DOME button</value>
        public Thread stop_dome_thread;
        /// <value>stall_thread is a thread waiting for stall detection</value>
        public Thread stall_thread;
        /// <value>weather_thread is a thread waiting for bad wheater detection</value>
        public Thread weather_thread;

        /// <value>az_conv is the AZ conversion constant</value>
        public const int az_conv = 52666;
        /// <value>alt_conv is the ALT conversion constant</value>
        public const int alt_conv = 351562;
        /// <value>shutter_conv is the SHUTTER conversion constant</value>
        public const int shutter_conv = 21066;
        /// <value>az_offset is AZ offset, by default to 0</value>
        public int az_offset = 0;
        /// <value>alt_offset is ALT offset, by default to 0</value>
        public int alt_offset = 0;
        /// <value>set_park_position is set park, by default to 180</value>
        public int set_park_position = 180;

        /// <value>is_dome_ready, by default to false</value>
        public bool is_dome_ready = false;
        /// <value>close dome, by default to false</value>
        public bool close = false;
        /// <value>weather_feature_setup, allows the user to activate the weather thread</value>
        public bool weather_feature_setup = false;
        /// <value>path_weather_file string</value>
        public string path_weather_file = @"C:\Users\ebondoux\Documents\Prism\01_METEO_DATA\recfile.txt";
        /// <value>path_config_file string</value>
        public string path_config_file = @"C:\Users\ebondoux\Documents\00_dev_OGS\test.txt";
        /// <value>config_data, composed of 8 values</value>
        public int[] config_data = { 5, 5, 0, 0, 180, 40, 80 };
        /// <value>weather, composed of 21 values</value>
        public string[] weather = new string[21];
        /// <value>humidity value, read in the weather file</value>
        public double humidity;
        /// <value>rain value, read in the weather file</value>
        public double rain;
        /// <value>wind value, read in the weather file</value>
        public double wind;
        /// <value>cloud condition, read in the weather file</value>
        public double cloud;
        /// <value>humidity_threshold value, read in config data file</value>
        public double humidity_threshold;
        /// <value>wind_threshold value, read in config data file</value>
        public double wind_threshold;

        /// <summary>
        /// Constructor Dome class.
        /// </summary>
        /// <remarks>
        /// Must be public for COM registration.
        /// Instantiates TraceLogger, Config and Util objects
        /// </remarks>
        public Dome()
        {
            tl = new TraceLogger("", "UNIDome");
            ReadProfile();
            connectedState = false;
            Config = new Config();
            utilities = new Util();
        }

        #region Common properties and methods

        /// <summary>
        /// Displays the Setup Dialog form.
        /// If the user clicks the OK button to dismiss the form, then
        /// the new settings are saved, otherwise the old values are reloaded.
        /// </summary>
        /// <remarks>
        /// Consider only showing the setup dialog if not connected
        /// or call a different dialog if connected
        /// </remarks>
        public void SetupDialog()
        {
            if (IsConnected)
                System.Windows.Forms.MessageBox.Show("Already connected, just press OK");

            using (SetupDialogForm F = new SetupDialogForm(tl))
            {
                var result = F.ShowDialog();                
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    WriteProfile();
                }
            }
        }

        /// <summary>
        /// This function creates an ArrayList of supported actions (NOT USED)
        /// </summary>
        /// <returns>
        /// ArrayList
        /// </returns>
        public ArrayList SupportedActions
        {
            get
            {
                return new ArrayList();
            }
        }

        /// <summary>
        /// This function creates actions (NOT USED)
        /// </summary>
        /// <returns>
        /// The action string (name and parameters)
        /// </returns>
        /// <exception cref="ASCOM.ActionNotImplementedException">
        /// Exception thrown because action is not implemented
        /// </exception>
        /// <param name="actionName">Name</param>
        /// <param name="actionParameters">Parameters</param>
        public string Action(string actionName, string actionParameters)
        {
            LogMessage("", "Action {0}, parameters {1} not implemented", actionName, actionParameters);
            throw new ASCOM.ActionNotImplementedException("Action " + actionName + " is not implemented");
        }

        /// <summary>
        /// This function transmits an arbitrary string to the device and
        /// does not wait for a response (NOT USED)
        /// </summary>
        /// <exception cref="ASCOM.MethodNotImplementedException">
        /// Thrown because method is not implemented
        /// </exception>
        /// <param name="command">Command</param>
        /// <param name="raw">raw</param>
        public void CommandBlind(string command, bool raw)
        {
            CheckConnected("CommandBlind");
            throw new ASCOM.MethodNotImplementedException("CommandBlind");
        }

        /// <summary>
        /// This function transmits an arbitrary string to the device and
        /// waits for a boolean response (NOT USED)
        /// </summary>
        /// <returns>
        /// Boolean response
        /// </returns>
        /// <exception cref="ASCOM.MethodNotImplementedException">
        /// Thrown because method is not implemented
        /// </exception>
        /// <param name="command">Command</param>
        /// <param name="raw">raw</param>
        public bool CommandBool(string command, bool raw)
        {
            CheckConnected("CommandBool");
            throw new ASCOM.MethodNotImplementedException("CommandBool");
        }

        /// <summary>
        /// This function transmits an arbitrary string to the device and
        /// waits for a string response (NOT USED)
        /// </summary>
        /// <returns>
        /// String response
        /// </returns>
        /// <exception cref="ASCOM.MethodNotImplementedException">
        /// Thrown because method is not implemented
        /// </exception>
        /// <param name="command">Command</param>
        /// <param name="raw">raw</param>
        public string CommandString(string command, bool raw)
        {
            CheckConnected("CommandString");
            throw new ASCOM.MethodNotImplementedException("CommandString");
        }

        /// <summary>
        /// Clean up and release trace logger, utilities object,
        /// serial connections and the dome_gui, tracking_on_thread,
        /// stop_dome_thread, stall_thread, weather_thread.
        /// </summary>
        public void Dispose()
        {
            tl.Enabled = false;
            tl.Dispose();
            tl = null;
            utilities.Dispose();
            utilities = null;
            SerialConnection._domeSerial.Close();
            SerialConnection._altSerial.Close();
            dome_gui.Abort();
            tracking_on_thread.Abort();
            stop_dome_thread.Abort();
            stall_thread.Abort();
            weather_thread.Abort();
        }

        /// <summary>
        /// Accessor and mutator of the connected status.
        /// In the mutator section, if the dome is connected the serial
        /// connection with the dome is instantiated and initialized.
        /// The windows form and the dome_gui, tracking_on_thread,
        /// stop_dome_thread, stall_thread, weather_thread are also
        /// started. Init parsing of config data and weather file is done.
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool Connected
        {
            get
            {
                LogMessage("Connected", "Get {0}", Config.IsConnected);
                return Config.IsConnected;
            }
            set
            {
                if (value == Config.IsConnected)
                    return;

                if (value)
                {
                    SetupDialogForm F = new SetupDialogForm(tl);
                    weather_feature_setup = F.weather_setup();
                    Config.IsConnected = true;
                    LogMessage("Connected Set", "Connecting to port {0}", comPortDefault);
                    unidome_form = new UNIDomeForm();
                    unidome_form.draw_box_rain(Convert.ToInt32(rain));
                    unidome_form.draw_box_cloud(Convert.ToInt32(cloud));
                    SerialConnection = new DomeSerial(comPortDefault);
                    SerialConnection.altSerial(comPortDefaultalt);
                    dome_gui = new Thread(new ThreadStart(run));
                    dome_gui.Start();
                    tracking_on_thread = new Thread(new ThreadStart(tracking_on_feature));
                    tracking_on_thread.Start();
                    stop_dome_thread = new Thread(new ThreadStart(stop_dome_feature));
                    stop_dome_thread.Start();

                    if (weather_feature_setup == true)
					{
                        weather_thread = new Thread(new ThreadStart(bad_weather_feature));
                        weather_thread.Start();
                    }                        
                    //stall_thread = new Thread(new ThreadStart(stall_feature));
                    //stall_thread.Start();
                }
                else
                {
                    Config.IsConnected = false;
                    LogMessage("Connected Set", "Disconnecting from port {0}", comPortDefault);
                }
            }
        }

        /// <summary>
        /// Accessor of the driver description
        /// </summary>
        /// <returns>
        /// String description
        /// </returns>
        public string Description
        {
            get
            {
                return driverDescription;
            }
        }

        /// <summary>
        /// Accessor of the driver info
        /// </summary>
        /// <returns>
        /// String info
        /// </returns>
        public string DriverInfo
        {
            get
            {
                Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                string driverInfo = "Information about the driver itself. Version: "
                + String.Format(CultureInfo.InvariantCulture, "{0}.{1}", version.Major, version.Minor);
                return driverInfo;
            }
        }

        /// <summary>
        /// Accessor of the driver version
        /// </summary>
        /// <returns>
        /// String version
        /// </returns>
        public string DriverVersion
        {
            get
            {
                Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                string driverVersion = String.Format(CultureInfo.InvariantCulture,
                            "{0}.{1}", version.Major, version.Minor);
                return driverVersion;
            }
        }

        /// <summary>
        /// Accessor of the interface version number supported
        /// </summary>
        /// <returns>
        /// Short number
        /// </returns>
        public short InterfaceVersion
        {
            get
            {
                LogMessage("InterfaceVersion Get", "2");
                return Convert.ToInt16("2");
            }
        }

        /// <summary>
        /// Accessor of the driver name
        /// </summary>
        /// <returns>
        /// String name
        /// </returns>
        public string Name
        {
            get
            {
                string name = "UNIDome";
                return name;
            }
        }

        #endregion //Common properties and methods

        #region IDome Implementation

        /// <summary>
        /// This function abort the motion by stopping
        /// all movements in AZ, ALT, SHUTTER
        /// </summary>
        public void AbortSlew()
        {
            string retval;
            retval = SerialConnection.SendCommand(SerialConnection.stop_az);
            Thread.Sleep(2000);
            retval = SerialConnection.SendCommand(SerialConnection.stop_alt);
            Thread.Sleep(2000);
            retval = SerialConnection.SendCommand(SerialConnection.stop_shutter);
            Thread.Sleep(2000);
            unidome_form.draw_green_box();
        }

        /// <summary>
        /// Accessor of the park boolean (TRUE)
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool AtPark
        {
            get { return true; }
        }

        /// <summary>
        /// Accessor and mutator of the slaved value (MUST NOT BE CHANGED)
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        /// <exception cref="ASCOM.PropertyNotImplementedException">
        /// Thrown because property is not implemented
        /// </exception>
        public bool Slaved
        {
            get
            {
                return false;
            }
            set
            {
                throw new ASCOM.PropertyNotImplementedException("Slaved", false);
            }
        }

        /// <summary>
        /// Accessor of the slewing value
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool Slewing
        {
            get
            {
                if ((Config.AzimuthDest == 0) &&
                    (Config.AltitudeDest == 0))
                {
                    return false;
                }
                else
                    return Config.IsSlewing;
            }
        }

        /// <summary>
        /// Accessor of the home boolean
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool AtHome
        {
            get { return Config.AtHome; }
        }

        /// <summary>
        /// Accessor of the altitude value
        /// </summary>
        /// <returns>
        /// Double value
        /// </returns>
        public double Altitude
        {
            get { return Config.AltitudeDest; }
        }

        /// <summary>
        /// Accessor of the azimuth value
        /// </summary>
        /// <returns>
        /// Double value
        /// </returns>
        public double Azimuth
        {
            get { return Config.AzimuthDest; }
        }

        /// <summary>
        /// Accessor of the Shutter Status
        /// </summary>
        /// <returns>
        /// ShutterState value (open, closed, opening, closing, error)
        /// </returns>
        public ShutterState ShutterStatus
        {
            get { return Config.ShutterStatus; }
        }

        /// <summary>
        /// CanSetPark feature (TRUE)
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool CanSetPark
        {
            get { return true; }
        }

        /// <summary>
        /// CanPark feature (TRUE)
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool CanPark
        {
            get { return true; }
        }

        /// <summary>
        /// CanSlave feature (TRUE)
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool CanSlave
        {
            get { return true; }
        }

        /// <summary>
        /// CanFindHome feature (TRUE)
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool CanFindHome
        {
            get { return true; }
        }

        /// <summary>
        /// CanSetAltitude feature (TRUE)
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool CanSetAltitude
        {
            get { return true; }
        }

        /// <summary>
        /// CanSetAzimuth feature (TRUE)
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool CanSetAzimuth
        {
            get { return true; }
        }

        /// <summary>
        /// CanSetShutter feature (TRUE)
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool CanSetShutter
        {
            get { return true; }
        }

        /// <summary>
        /// CanSyncAzimuth feature (TRUE)
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool CanSyncAzimuth
        {
            get { return true; }
        }

        /// <summary>
        /// This function sends the Park command.
        /// Slew to Config.AzimuthDest azimuth value.
        /// Update also the corresponding label in the Form
        /// when the moving flag is at 0 by asking the dome
        /// the real value and convert it in degrees.
        /// </summary>
        public void Park()
        {
            double AZPAS;
            string AZPASStr;
            string retval;

            AZPAS = set_park_position * az_conv;
            AZPASStr = AZPAS.ToString();
            retval = SerialConnection.SendCommand(SerialConnection.move_abs_az, AZPASStr);
            Config.IsSlewing = true;
            unidome_form.draw_red_box();
            Thread.Sleep(2000);
            moving_flag(SerialConnection.mv_flag_az);

            retval = SerialConnection.SendCommand(SerialConnection.position_flag_az);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("park " + StringToInt(retval));
            AZPAS = Convert.ToInt32(StringToInt(retval)) / az_conv;
            unidome_form.label_az_set(Convert.ToString(AZPAS));
            Config.AzimuthDest = AZPAS;
            Config.IsSlewing = false;
            unidome_form.draw_green_box();
        }

        /// <summary>
        /// Set the current azimuth position of the dome to the
        /// park position (NOT USED)
        /// </summary>
        public void SetPark()
        {
        }

        /// <summary>
        /// This function sends the close shutter command through serial
        /// connection and set the shutter status to closed when the moving
        /// flag is at 0 by asking the dome the real value and convert
        /// it in %. Update also the corresponding label in the Form.
        /// </summary>
        public void CloseShutter()
        {
            double value;
            string retval;
            Config.ShutterValue = Config.ShutterValue * (-1);
            retval = SerialConnection.SendCommand(SerialConnection.CloseShutter,
                Config.ShutterValue.ToString());
            Thread.Sleep(2000);
            unidome_form.draw_red_box();

            moving_flag(SerialConnection.mv_flag_shutter);
            retval = SerialConnection.SendCommand(SerialConnection.position_flag_shutter);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("close shutter " + StringToInt(retval));
            value = Convert.ToInt32(StringToInt(retval)) / shutter_conv;
            unidome_form.label_shutter_set(Convert.ToString(value));
            Config.ShutterValue = value;
            Config.ShutterStatus = ShutterState.shutterClosed;
            unidome_form.draw_green_box();
        }

        /// <summary>
        /// This function sends the open shutter command through serial
        /// connection and set the shutter status to open when the moving
        /// flag is at 0 by asking the dome the real value and convert
        /// it in %. Update also the corresponding label in the Form.
        /// </summary>
        public void OpenShutter()
        {
            string retval;
            double value;
            retval = SerialConnection.SendCommand(SerialConnection.OpenShutter,
                Config.ShutterValue.ToString());
            Thread.Sleep(2000);
            unidome_form.draw_red_box();

            moving_flag(SerialConnection.mv_flag_shutter);
            retval = SerialConnection.SendCommand(SerialConnection.position_flag_shutter);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("open shutter " + StringToInt(retval));
            value = Convert.ToInt32(StringToInt(retval)) / shutter_conv;
            unidome_form.label_shutter_set(Convert.ToString(value));
            Config.ShutterValue = value;
            Config.ShutterStatus = ShutterState.shutterOpen;
            unidome_form.draw_green_box();
        }

        /// <summary>
        /// This function sends the home command through
        /// serial connection and update the corresponding
        /// label in the Form when the moving flag is at 0
        /// by asking the dome the real value and convert
        /// it in degrees.
        /// </summary>
        public void FindHome()
        {
            double az_value;
            string retval;
            retval = SerialConnection.SendCommand(SerialConnection.home_az);
            Config.IsSlewing = true;
            unidome_form.draw_red_box();
            Thread.Sleep(2000);

            moving_flag(SerialConnection.mv_flag_az);
            retval = SerialConnection.SendCommand(SerialConnection.position_flag_az);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("FindHome " + StringToInt(retval));
            az_value = Convert.ToInt32(StringToInt(retval)) / az_conv;
            unidome_form.label_az_set(Convert.ToString(az_value));
            Config.AzimuthDest = az_value;
            Config.AtHome = true;
            Config.IsSlewing = false;
            unidome_form.draw_green_box();
        }

        /// <summary>
        /// This function sends move_abs_alt command through serial connection.
        /// Update also the corresponding label in the Form when the moving
        /// flag is at 0 by asking the dome the real value and convert
        /// it in degrees.
        /// </summary>
        /// <param name="Altitude">Altitude value</param>
        public void SlewToAltitude(double Altitude)
        {
            double ALTPAS;
            string ALTPASStr;
            string retval;
            ALTPAS = Altitude * alt_conv;
            ALTPASStr = ALTPAS.ToString();
            retval = SerialConnection.SendCommand(SerialConnection.move_abs_alt, ALTPASStr);
            Config.IsSlewing = true;
            unidome_form.draw_red_box();
            Thread.Sleep(2000);

            moving_flag(SerialConnection.mv_flag_alt);
            retval = SerialConnection.SendCommand(SerialConnection.position_flag_alt);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("slew to alt " + StringToInt(retval));
            ALTPAS = Convert.ToInt32(StringToInt(retval)) / alt_conv;
            unidome_form.label_alt_set(Convert.ToString(ALTPAS));
            Config.AltitudeDest = ALTPAS;
            Config.IsSlewing = false;
            unidome_form.draw_green_box();
        }

        /// <summary>
        /// This function sends move_abs_az command through serial connection.
        /// It sends also move_abs_alt command for tracking with the telescope
        /// in PRISME.
        /// </summary>
        /// <param name="azimuth">azimuth value</param>
        public void SlewToAzimuth(double azimuth)
        {
            double AZPAS;
            double ALTPAS;
            string AZPASStr;
            string retval;
            string retval_alt;

            AZPAS = azimuth * az_conv;
            AZPASStr = AZPAS.ToString();
            retval = SerialConnection.SendCommand(SerialConnection.move_abs_az, AZPASStr);
            utilities.WaitForMilliseconds(100);
            unidome_form.textBox_debug_set("sent command az");
            moving_flag(SerialConnection.mv_flag_az);

            SerialConnection._altSerial.DiscardInBuffer();
            retval_alt = SerialConnection._altSerial.ReadLine();
            unidome_form.textBox_debug_set("altitude from inclino " + retval_alt);
            ALTPAS = Int16.Parse(retval_alt) * alt_conv;
            retval = SerialConnection.SendCommand(SerialConnection.move_abs_alt, ALTPAS.ToString());
            utilities.WaitForMilliseconds(100);
            unidome_form.textBox_debug_set("sent command alt");
            
            moving_flag(SerialConnection.mv_flag_alt);
            Config.AzimuthDest = AZPAS;
            Config.AltitudeDest = ALTPAS;
        }

        /// <summary>
        /// This function sends SyncToAzimuth command through serial connection
        /// Update also the corresponding label in the Form by asking the dome
        /// the real value and convert it in degrees.
        /// </summary>
        /// <param name="Azimuth">Azimuth value</param>
        public void SyncToAzimuth(double Azimuth)
        {
            string retval;
            double AZPAS;
            retval = SerialConnection.SendCommand(SerialConnection.SyncToAzimuth, Azimuth.ToString());
            Thread.Sleep(2000);

            retval = SerialConnection.SendCommand(SerialConnection.position_flag_az);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("sync to az " + StringToInt(retval));
            AZPAS = Convert.ToInt32(StringToInt(retval)) / az_conv;
            unidome_form.label_az_set(Convert.ToString(AZPAS));
            Config.AzimuthDest = AZPAS;
        }

        #endregion //IDome Implementation

        #region Private properties and methods

        #region ASCOM Registration

        /// <summary>
        /// Register or unregister the driver with the ASCOM Platform.
        /// This is harmless if the driver is already registered/unregistered.
        /// </summary>
        /// <param name="bRegister">If true registers the driver otherwise
        /// unregisters it</param>
        private static void RegUnregASCOM(bool bRegister)
        {
            using (var P = new ASCOM.Utilities.Profile())
            {
                P.DeviceType = "Dome";
                if (bRegister)
                {
                    P.Register(driverID, driverDescription);
                }
                else
                {
                    P.Unregister(driverID);
                }
            }
        }

        /// <summary>
        /// This function registers the driver with the ASCOM Chooser and is
        /// called automatically whenever the class is registered for COM Interop
        /// </summary>
        /// <param name="t">Type of the class being registered (NOT USED)</param>
        /// <remarks>
        /// This method typically runs in two distinct situations:
        /// <list type="numbered">
        /// <item>
        /// In Visual Studio, when the project is successfully built.
        /// For this to work correctly, the option Register for COM Interop
        /// must be enabled in the project settings.
        /// </item>
        /// <item> During setup, when the installer registers the assembly
        /// for COM Interop.</item>
        /// </list>
        /// This technique should mean that it is never necessary to manually
        /// register a driver with ASCOM.
        /// </remarks>
        [ComRegisterFunction]
        public static void RegisterASCOM(Type t)
        {
            RegUnregASCOM(true);
        }

        /// <summary>
        /// This function unregisters the driver with the ASCOM Chooser and is
        /// called automatically whenever the class is unregistered for COM Interop
        /// </summary>
        /// <param name="t">Type of the class being registered (NOT USED)</param>
        /// <remarks>
        /// This method typically runs in two distinct situations:
        /// <list type="numbered">
        /// <item>
        /// In Visual Studio, when the project is cleaned or prior to rebuilding.
        /// For this to work correctly, the option Register for COM Interop
        /// must be enabled in the project settings.
        /// </item>
        /// <item> During uninstall, when the installer unregisters assembly
        /// from COM Interop.</item>
        /// </list>
        /// This technique should mean that it is never necessary to manually
        /// unregister a driver from ASCOM.
        /// </remarks>
        [ComUnregisterFunction]
        public static void UnregisterASCOM(Type t)
        {
            RegUnregASCOM(false);
        }

        #endregion //ASCOM Registration

        /// <summary>
        /// Returns true if there is a valid connection to the driver hardware
        /// </summary>
        /// <returns>
        /// Boolean value
        /// </returns>
        private bool IsConnected
        {
            get
            {
                connectedState = Config.IsConnected;
                return connectedState;
            }
        }

        /// <summary>
        /// This function throw an exception if hardware is not connected
        /// </summary>
        /// <exception cref="ASCOM.NotConnectedException">
        /// Thrown because hardware is not connected
        /// </exception>
        /// <param name="message">Message string</param>
        private void CheckConnected(string message)
        {
            if (!Config.IsConnected)
                throw new ASCOM.NotConnectedException(message);
        }

        /// <summary>
        /// This function parse the data weather file and
        /// extract rain, humidity, wind and cloud information
        /// from it. Construct an array of string with the data
        /// separated with a space character.
        /// </summary>
        /// <remarks>
        /// The extension file MUST NOT be added in the file name
        /// in Windows OS.
        /// </remarks>
        /// <param name="path">path string</param>
        private void weather_parsing(string path)
        {
            int i = 0;
            if (File.Exists(path))
            {
                string appendText = " ";
                File.AppendAllText(path, appendText);
                string[] lines = File.ReadAllLines(path);
                string word = "";
                foreach (string l in lines)
                {
                    char[] characters = l.ToCharArray();
                    foreach (char c in characters)
                    {
                        if (c != ' ')
                        {
                            word += c;
                        }
                        else
                        {
                            if (!word.Equals(""))
                            {
                                weather[i] = word;
                                i++;
                            }
                            word = "";
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This function read the config data file and
        /// extract AZ velocity, ALT velocity, 
        /// ALT offset, AZ offset, set park position,
        /// wind and humidity. Fill the config_data array of int.
        /// </summary>
        /// <remarks>
        /// The extension file MUST NOT be added in the file name
        /// in Windows OS.
        /// </remarks>
        /// <param name="path">path string</param>
        private void config_data_read(string path)
        {
            int i = 0;
            if (File.Exists(path))
            {
                string[] lines = File.ReadAllLines(path);
                foreach (string l in lines)
                {
                    config_data[i] = Convert.ToInt32(l);
                    i++;
                }
            }
        }

        /// <summary>
        /// This function writes the config data in the file
        /// Use the config_data array of int to update the values.
        /// </summary>
        /// <remarks>
        /// The extension file MUST NOT be added in the file name
        /// in Windows OS.
        /// </remarks>
        /// <param name="path">path string</param>
        private void config_data_write(string path)
        {
            int i = 0;
            string[] str = new string[7];
            if (File.Exists(path))
            {
                foreach (int val in config_data)
                {
                    str[i] = val.ToString();
                    i++;
                }
                File.WriteAllLines(path, str);
            }
        }

        /// <summary>
        /// This function read the config data and fill the array.
        /// Parse also the weather file and extract rain, wind, 
        /// humidity and cloud. Update the corresponding labels.
        /// </summary>
        private void init_parsing(string path_weather, string path_config)
        {
            config_data_read(path_config);
            weather_parsing(path_weather);
            wind = Convert.ToDouble(weather[7],
                System.Globalization.CultureInfo.InvariantCulture);
            humidity = Convert.ToDouble(weather[8],
                System.Globalization.CultureInfo.InvariantCulture);
            rain = Convert.ToDouble(weather[11],
                System.Globalization.CultureInfo.InvariantCulture);
            cloud = Convert.ToDouble(weather[15],
                System.Globalization.CultureInfo.InvariantCulture);
            wind_threshold = Convert.ToDouble(config_data[5],
                System.Globalization.CultureInfo.InvariantCulture);
            humidity_threshold = Convert.ToDouble(config_data[6],
                System.Globalization.CultureInfo.InvariantCulture);
            unidome_form.draw_box_rain(Convert.ToInt32(rain));
            unidome_form.draw_box_cloud(Convert.ToInt32(cloud));
            unidome_form.label_wind_set(wind.ToString());
            unidome_form.label_wind_threshold_set(wind_threshold.ToString());
            unidome_form.label_humidity_set(humidity.ToString());
            unidome_form.label_humidity_threshold_set(humidity_threshold.ToString());
        }

        /// <summary>
        /// This function stop the dome if the conditions are bad
        /// (wind > threshold, humidity > threshold, raining).
        /// Note that this method is associated to weather_thread
        /// and check data every 5s.
        /// </summary>
        /// <remarks>
        /// Use Thread.Sleep function instead of ASCOM utilities
        /// (wait, logging...)
        /// </remarks>
        private void bad_weather_feature()
        {
            while (true)
            {
                init_parsing(path_weather_file, path_config_file);
                if ((wind > wind_threshold) ||
                    (humidity > humidity_threshold) ||
                    (rain == 2.0) ||
                    (rain == 1.0))
                {
                    if (close == false)
                    {
                        unidome_form.bad_weather_condition_warning();
                        AbortSlew();
                        Thread.Sleep(2000);
                        SlewToAltitude(-90 + alt_offset);
                        Thread.Sleep(2000);
                        Config.ShutterValue = 100 * shutter_conv;
                        CloseShutter();
                        Thread.Sleep(2000);
                        Park();
                        close = true;
                    }
                }
                else
                {
                    if (close == true)
                        close = false;
                }
                Thread.Sleep(5000);
            }
        }

        /// <summary>
        /// This function is waiting for user to click on the
        /// MOTOR ACTIVATION button. This is a blocking
        /// process until the user force the opening of the dome
        /// (security feature).
        /// </summary>
        private void motor_activation_feature()
        {
            while (true)
            {
                if (unidome_form.bool_bt_motor_activation == true)
                {
                    unidome_form.bool_bt_motor_activation = false;
                    break;
                }
            }
        }

        /// <summary>
        /// This function is waiting for user to click on the
        /// FULL HOME button. This is a blocking process until
        /// the user force the homing of the dome
        /// for AZ, ALT and SHUTTER (security feature).
        /// </summary>
        private void home_feature()
        {
            while (true)
            {
                if ((unidome_form.bool_bt_full_home) == true && (close == false))
                {
                    SlewToAltitude(-90 + alt_offset);
                    Thread.Sleep(2000);
                    Config.ShutterValue = 100 * shutter_conv;
                    CloseShutter();
                    Thread.Sleep(2000);
                    FindHome();
                    unidome_form.bool_bt_full_home = false;
                    break;
                }
            }
        }

        /// <summary>
        /// This function is waiting for user to click on
        /// TRACKING ON button. Note that this method is
        /// associated to the tracking_on_thread and uses
        /// the SlewToAzimuth driver function.
        /// </summary>
        /// <remarks>
        /// If the TRACKING OFF button is clicked the tracking
        /// is stopped and the thread is waiting again for click
        /// on TRACKING ON.
        /// </remarks>
        private void tracking_on_feature()
        {
            while (true)
            {
                if ((is_dome_ready == true) && (close == false))
                {
                    if (unidome_form.bool_bt_tracking_on == true)
                    {
                        SlewToAzimuth(180 + az_offset);

                    }
                    if (unidome_form.bool_bt_tracking_off == true)
                    {
                        unidome_form.bool_bt_tracking_on = false;
                        unidome_form.bool_bt_tracking_off = false;
                    }
                }
            }
        }

        /// <summary>
        /// This function is waiting for user to click on
        /// STOP DOME button. Note that this method is
        /// associated to the stop_dome_thread and uses
        /// the AbortSlew driver function.
        /// </summary>
        private void stop_dome_feature()
        {
            if ((is_dome_ready == true) && (close == false))
            {
                if (unidome_form.bool_bt_stop_dome == true)
                {
                    AbortSlew();
                    unidome_form.bool_bt_stop_dome = false;
                }
            }
        }

        /// <summary>
        /// This function is waiting for stall detection.
        /// Note that this method is associated to the
        /// stall_thread and send the stall flag command.
        /// </summary>
        private void stall_feature()
        {
            string retval;
            string value;

            while (true)
            {
                retval = SerialConnection.SendCommand(SerialConnection.stall_flag_az);
                retval = SerialConnection._domeSerial.ReadLine();
                value = StringToInt(retval);
                unidome_form.textBox_debug_set("stall_flag AZ " + value);
                Thread.Sleep(1000);
                if ((value.Equals("0")) || (value.Equals("1")))
                {
                    if (value.Equals("1"))
                    {
                        unidome_form.label_stall_az_set("stall az TRUE");
                    }
                }

                retval = SerialConnection.SendCommand(SerialConnection.stall_flag_alt);
                retval = SerialConnection._domeSerial.ReadLine();
                value = StringToInt(retval);
                unidome_form.textBox_debug_set("stall_flag ALT " + value);
                Thread.Sleep(1000);
                if ((value.Equals("0")) || (value.Equals("1")))
                {
                    if (value.Equals("1"))
                    {
                        unidome_form.label_stall_alt_set("stall alt TRUE");
                    }
                }

                retval = SerialConnection.SendCommand(SerialConnection.stall_flag_shutter);
                retval = SerialConnection._domeSerial.ReadLine();
                value = StringToInt(retval);
                unidome_form.textBox_debug_set("stall_flag SHUTTER " + value);
                Thread.Sleep(1000);
                if ((value.Equals("0")) || (value.Equals("1")))
                {
                    if (value.Equals("1"))
                    {
                        unidome_form.label_stall_shutter_set("stall shutter TRUE");
                    }
                }
            }
        }

        /// <summary>
        /// This function is waiting for user input/output in the GUI
        /// (click buttons) to call the appropriate functions.
        /// Note that this method is associated to dome_gui thread.
        /// It also manages the blocking functions for motor activation
        /// and homing buttons.
        /// </summary>
        /// <remarks>
        /// The dome is ready to be used after the user has clicked on
        /// MOTOR ACTIVATION and FULL HOME buttons.
        /// </remarks>
        private void run()
        {
            string retval;
            int mv;

            motor_activation_feature();
            home_feature();
            is_dome_ready = true;

            while (true)
            {
                if (close == false)
                {
                    if (unidome_form.bool_bt_home_az == true)
                    {
                        FindHome();
                        unidome_form.bool_bt_home_az = false;
                    }
                    if (unidome_form.bool_bt_home_alt == true)
                    {
                        SlewToAltitude(-90 + alt_offset);
                        unidome_form.bool_bt_home_alt = false;
                    }
                    if (unidome_form.bool_bt_home_shutter == true)
                    {
                        Config.ShutterValue = 100 * shutter_conv;
                        CloseShutter();
                        unidome_form.bool_bt_home_shutter = false;
                    }
                    if (unidome_form.bool_bt_sync_az == true)
                    {
                        mv = unidome_form.upDown_sync_az_get();
                        SyncToAzimuth((mv + az_offset) * az_conv);
                        unidome_form.bool_bt_sync_az = false;
                    }
                    if (unidome_form.bool_bt_sync_alt == true)
                    {
                        mv = unidome_form.upDown_sync_alt_get();
                        SyncToAltitude((mv + alt_offset) * alt_conv);
                        unidome_form.bool_bt_sync_alt = false;
                    }
                    if (unidome_form.bool_bt_sync_shutter == true)
                    {
                        mv = unidome_form.upDown_sync_shutter_get();
                        SyncToShutter(mv * shutter_conv);
                        unidome_form.bool_bt_sync_shutter = false;
                    }
                    if (unidome_form.bool_bt_back_up == true)
                    {
                        unidome_form.bool_bt_back_up = false;
                    }
                    if (unidome_form.bool_bt_log == true)
                    {
                        log_infos();
                        unidome_form.bool_bt_log = false;
                    }
                    if (unidome_form.bool_bt_east_abs_az == true)
                    {
                        mv = unidome_form.upDown_abs_az_get();
                        SlewToAzimuth_abs(mv + az_offset);
                        unidome_form.bool_bt_east_abs_az = false;
                    }
                    if (unidome_form.bool_bt_east_abs_alt == true)
                    {
                        mv = unidome_form.upDown_abs_alt_get();
                        SlewToAltitude(mv + alt_offset);
                        unidome_form.bool_bt_east_abs_alt = false;
                    }
                    if (unidome_form.bool_bt_west_abs_alt == true)
                    {
                        mv = unidome_form.upDown_abs_alt_get();
                        SlewToAltitude((mv + alt_offset) * (-1));
                        unidome_form.bool_bt_west_abs_alt = false;
                    }
                    if (unidome_form.bool_bt_east_relat_az == true)
                    {
                        mv = unidome_form.upDown_relat_az_get();
                        SlewToAzimuth_relat(mv + az_offset);
                        unidome_form.bool_bt_east_relat_az = false;
                    }
                    if (unidome_form.bool_bt_east_relat_alt == true)
                    {
                        mv = unidome_form.upDown_relat_alt_get();
                        SlewToAltitude_relat(mv + alt_offset);
                        unidome_form.bool_bt_east_relat_alt = false;
                    }
                    if (unidome_form.bool_bt_west_relat_az == true)
                    {
                        mv = unidome_form.upDown_relat_az_get();
                        SlewToAzimuth_relat((mv + az_offset) * (-1));
                        unidome_form.bool_bt_west_relat_az = false;
                    }
                    if (unidome_form.bool_bt_west_relat_alt == true)
                    {
                        mv = unidome_form.upDown_relat_alt_get();
                        SlewToAltitude_relat((mv + alt_offset) * (-1));
                        unidome_form.bool_bt_west_relat_alt = false;
                    }
                    if (unidome_form.bool_bt_open_shutter == true)
                    {
                        mv = unidome_form.upDown_shutter_get();
                        Config.ShutterValue = mv * shutter_conv;
                        OpenShutter();
                        unidome_form.bool_bt_open_shutter = false;
                    }
                    if (unidome_form.bool_bt_close_shutter == true)
                    {
                        mv = unidome_form.upDown_shutter_get();
                        Config.ShutterValue = mv * shutter_conv;
                        CloseShutter();
                        unidome_form.bool_bt_close_shutter = false;
                    }
                    if (unidome_form.bool_bt_echo_off == true)
                    {
                        retval = SerialConnection.SendCommand(SerialConnection.echo_mode_off);
                        unidome_form.bool_bt_echo_off = false;
                    }
                    if (unidome_form.bool_bt_echo_on == true)
                    {
                        retval = SerialConnection.SendCommand(SerialConnection.echo_mode_on);
                        unidome_form.bool_bt_echo_on = false;
                    }
                    if (unidome_form.bool_bt_park == true)
                    {
                        Config.ShutterValue = 100 * shutter_conv;
                        CloseShutter();
                        Thread.Sleep(2000);
                        SlewToAltitude(-90 + alt_offset);
                        Thread.Sleep(2000);
                        Park();
                        unidome_form.bool_bt_park = false;
                    }
                    if (unidome_form.bool_bt_stop_com == true)
                    {
                        SerialConnection._altSerial.Close();
                        unidome_form.bool_bt_stop_com = false;
                    }
                    if (unidome_form.bool_bt_init_com == true)
                    {
                        SerialConnection.altSerial(comPortDefaultalt);
                        unidome_form.bool_bt_init_com = false;
                    }
                    if (unidome_form.bool_bt_display == true)
                    {
                        display_position();
                        foreach (int i in config_data)
                            unidome_form.textBox_debug_set(i.ToString());
                        unidome_form.bool_bt_display = false;
                    }
                    if (unidome_form.bool_bt_ok_velocity_az == true)
                    {
                        mv = unidome_form.upDown_velocity_az_get() * az_conv;
                        retval = SerialConnection.SendCommand(SerialConnection.velocity_az,
                            mv.ToString());
                        config_data[0] = unidome_form.upDown_velocity_az_get();
                        config_data_write(path_config_file);
                        unidome_form.textBox_debug_set("config velocity AZ: " + config_data[0]);
                        unidome_form.bool_bt_ok_velocity_az = false;
                    }
                    if (unidome_form.bool_bt_ok_velocity_alt == true)
                    {
                        mv = unidome_form.upDown_velocity_alt_get() * alt_conv;
                        retval = SerialConnection.SendCommand(SerialConnection.velocity_alt,
                            mv.ToString());
                        config_data[1] = unidome_form.upDown_velocity_alt_get();
                        config_data_write(path_config_file);
                        unidome_form.textBox_debug_set("config velocity ALT: " + config_data[1]);
                        unidome_form.bool_bt_ok_velocity_alt = false;
                    }
                    if (unidome_form.bool_bt_ok_az == true)
                    {
                        az_offset = unidome_form.upDown_az_offset_get();
                        config_data[3] = az_offset;
                        config_data_write(path_config_file);
                        unidome_form.textBox_debug_set("config AZ position: " + config_data[3]);
                        unidome_form.bool_bt_ok_az = false;
                    }
                    if (unidome_form.bool_bt_ok_alt == true)
                    {
                        alt_offset = unidome_form.upDown_alt_offset_get();
                        config_data[2] = alt_offset;
                        config_data_write(path_config_file);
                        unidome_form.textBox_debug_set("config ALT position: " + config_data[2]);
                        unidome_form.bool_bt_ok_alt = false;
                    }
                    if (unidome_form.bool_bt_ok_set_park == true)
                    {
                        set_park_position = unidome_form.upDown_set_park_get();
                        config_data[4] = set_park_position;
                        config_data_write(path_config_file);
                        unidome_form.textBox_debug_set("config set park position: " + config_data[4]);
                        unidome_form.bool_bt_ok_set_park = false;
                    }
                    if (unidome_form.bool_bt_ok_wind == true)
                    {
                        config_data[5] = unidome_form.upDown_wind_get();
                        config_data_write(path_config_file);
                        unidome_form.textBox_debug_set("config wind: " + config_data[5]);
                        unidome_form.bool_bt_ok_wind = false;
                    }
                    if (unidome_form.bool_bt_ok_humidity == true)
                    {
                        config_data[6] = unidome_form.upDown_humidity_get();
                        config_data_write(path_config_file);
                        unidome_form.textBox_debug_set("config humidity: " + config_data[6]);
                        unidome_form.bool_bt_ok_humidity = false;
                    }
                    if (unidome_form.bool_bt_weather_path == true)
                    {
                        unidome_form.textBox_debug_set(unidome_form.weather_file_path_get());
                        path_weather_file = unidome_form.weather_file_path_get();
                        unidome_form.bool_bt_weather_path = false;
                    }
                    if (unidome_form.bool_bt_config_path == true)
                    {
                        unidome_form.textBox_debug_set(unidome_form.config_file_path_get());
                        path_config_file = unidome_form.config_file_path_get();
                        unidome_form.bool_bt_config_path = false;
                    }
                    if (unidome_form.bool_bt_stall_flag == true)
                    {
                        /*retval = SerialConnection.SendCommand(SerialConnection.stall_flag_az);
                        retval = SerialConnection._domeSerial.ReadLine();
                        unidome_form.textBox_debug_set("stall_flag AZ " + StringToInt(retval));
                        Thread.Sleep(2000);

                        retval = SerialConnection.SendCommand(SerialConnection.stall_flag_alt);
                        retval = SerialConnection._domeSerial.ReadLine();
                        unidome_form.textBox_debug_set("stall_flag ALT " + StringToInt(retval));
                        Thread.Sleep(2000);

                        retval = SerialConnection.SendCommand(SerialConnection.stall_flag_shutter);
                        retval = SerialConnection._domeSerial.ReadLine();
                        unidome_form.textBox_debug_set("stall_flag SHUTTER " + StringToInt(retval));
                        unidome_form.bool_bt_stall_flag = false;*/
                    }
                }
            }
        }

        /// <summary>
        /// This function sends all_flag_az command through serial connection.
        /// Add also this value in the corresponding textBox in the LOG Form.
        /// </summary>
        private void log_infos()
        {
            string retval;
            retval = SerialConnection.SendCommand(SerialConnection.all_flag_az);
            Thread.Sleep(2000);
            unidome_form.textBox_log_set(retval);
        }

        /// <summary>
        /// This function display the current AZ, ALT, SHUTTER
        /// position in the associated labels.
        /// </summary>
        private void display_position()
        {
            string retval;

            retval = SerialConnection.SendCommand(SerialConnection.position_flag_az);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("display az " + StringToInt(retval));
            Config.AzimuthDest = Convert.ToInt32(StringToInt(retval)) / az_conv;
            unidome_form.label_az_set(Convert.ToString(Config.AzimuthDest));
            Thread.Sleep(2000);

            retval = SerialConnection.SendCommand(SerialConnection.position_flag_alt);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("display alt " + StringToInt(retval));
            Config.AltitudeDest = Convert.ToInt32(StringToInt(retval)) / alt_conv;
            unidome_form.label_alt_set(Convert.ToString(Config.AltitudeDest));
            Thread.Sleep(2000);

            retval = SerialConnection.SendCommand(SerialConnection.position_flag_shutter);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("display shutter " + StringToInt(retval));
            Config.ShutterValue = Convert.ToInt32(StringToInt(retval)) / shutter_conv;
            unidome_form.label_shutter_set(Convert.ToString(Config.ShutterValue));
        }

        /// <summary>
        /// This function sends SyncToAltitude command through serial connection
        /// Update also the corresponding label in the Form by asking the dome
        /// the real value and convert it in degrees.
        /// </summary>
        /// <param name="Altitude">Altitude value</param>
        public void SyncToAltitude(double Altitude)
        {
            string retval;
            double ALTPAS;
            retval = SerialConnection.SendCommand(SerialConnection.SyncToAltitude, Altitude.ToString());
            Thread.Sleep(2000);

            retval = SerialConnection.SendCommand(SerialConnection.position_flag_alt);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("sync to alt " + StringToInt(retval));
            ALTPAS = Convert.ToInt32(StringToInt(retval)) / alt_conv;
            unidome_form.label_alt_set(Convert.ToString(ALTPAS));
            Config.AltitudeDest = ALTPAS;
        }

        /// <summary>
        /// This function sends SyncToShutter command through serial connection
        /// Update also the corresponding label in the Form by asking the dome
        /// the real value and convert it in degrees.
        /// </summary>
        /// <param name="value">Shutter value</param>
        public void SyncToShutter(double value)
        {
            string retval;
            double shutter_val;
            retval = SerialConnection.SendCommand(SerialConnection.SyncToShutter, value.ToString());
            Thread.Sleep(2000);

            retval = SerialConnection.SendCommand(SerialConnection.position_flag_shutter);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("sync to shutter " + StringToInt(retval));
            shutter_val = Convert.ToInt32(StringToInt(retval)) / alt_conv;
            unidome_form.label_shutter_set(Convert.ToString(shutter_val));
            Config.ShutterValue = shutter_val;
        }

        /// <summary>
        /// This function sends move_relat_alt command through serial connection.
        /// Update also the corresponding label in the Form when the moving
        /// flag is at 0 by asking the dome the real value and convert
        /// it in degrees.
        /// </summary>
        /// <param name="Altitude">Altitude value</param>
        public void SlewToAltitude_relat(double Altitude)
        {
            double ALTPAS;
            string ALTPASStr;
            string retval;
            ALTPAS = Altitude * alt_conv;
            ALTPASStr = ALTPAS.ToString();
            retval = SerialConnection.SendCommand(SerialConnection.move_relat_alt, ALTPASStr);
            Config.IsSlewing = true;
            unidome_form.draw_red_box();
            Thread.Sleep(2000);

            moving_flag(SerialConnection.mv_flag_alt);
            retval = SerialConnection.SendCommand(SerialConnection.position_flag_alt);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("slew to alt relat " + StringToInt(retval));
            ALTPAS = Convert.ToInt32(StringToInt(retval)) / alt_conv;
            unidome_form.label_alt_set(Convert.ToString(ALTPAS));
            Config.AltitudeDest = ALTPAS;
            Config.IsSlewing = false;
            unidome_form.draw_green_box();
        }

        /// <summary>
        /// This function sends move_relat_az command through serial connection.
        /// Update also the corresponding label in the Form when the moving
        /// flag is at 0 by asking the dome the real value and convert
        /// it in degrees.
        /// </summary>
        /// <param name="azimuth">azimuth value</param>
        public void SlewToAzimuth_relat(double azimuth)
        {
            double AZPAS;
            string AZPASStr;
            string retval;
            AZPAS = azimuth * az_conv;
            AZPASStr = AZPAS.ToString();
            retval = SerialConnection.SendCommand(SerialConnection.move_relat_az, AZPASStr);
            Config.IsSlewing = true;
            unidome_form.draw_red_box();
            Thread.Sleep(2000);

            moving_flag(SerialConnection.mv_flag_az);
            retval = SerialConnection.SendCommand(SerialConnection.position_flag_az);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("slew to az relat " + StringToInt(retval));
            AZPAS = Convert.ToInt32(StringToInt(retval)) / az_conv;
            unidome_form.label_az_set(Convert.ToString(AZPAS));
            Config.AzimuthDest = AZPAS;
            Config.IsSlewing = false;
            unidome_form.draw_green_box();
        }

        /// <summary>
        /// This function sends move_abs_az command through serial connection.
        /// Update also the corresponding label in the Form when the moving
        /// flag is at 0 by asking the dome the real value and convert
        /// it in degrees.
        /// </summary>
        /// <param name="azimuth">azimuth value</param>
        public void SlewToAzimuth_abs(double azimuth)
        {
            double AZPAS;
            string AZPASStr;
            string retval;
            AZPAS = azimuth * az_conv;
            AZPASStr = AZPAS.ToString();
            retval = SerialConnection.SendCommand(SerialConnection.move_abs_az, AZPASStr);
            Thread.Sleep(2000);
            Config.IsSlewing = true;
            unidome_form.draw_red_box();

            moving_flag(SerialConnection.mv_flag_az);
            retval = SerialConnection.SendCommand(SerialConnection.position_flag_az);
            retval = SerialConnection._domeSerial.ReadLine();
            unidome_form.textBox_debug_set("slew to az abs " + StringToInt(retval));
            AZPAS = Convert.ToInt32(StringToInt(retval)) / az_conv;
            unidome_form.label_az_set(Convert.ToString(AZPAS));
            Config.AzimuthDest = AZPAS;
            Config.IsSlewing = false;
            unidome_form.draw_green_box();
        }

        /// <summary>
        /// This function converts the string received from the dome 
        /// as a response into an numeric string. Check
        /// for numeric characters (0-9) or a negative sign.
        /// </summary>
        /// <returns>
        /// string
        /// </returns>
        /// <param name="str">String</param>
        private string StringToInt(string str)
        {
            string numericString = "";
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || c == '-')
                {
                    numericString = string.Concat(numericString, c);
                }
            }
            return numericString;
        }

        /// <summary>
        /// This function waits until the dome is not moving
        /// (value 0), uses serial communication to read the
        /// response of the dome every 1s
        /// </summary>
        /// <remarks>
        /// DiscardInBuffer MUST be used after ReadLine.
        /// </remarks>
        /// <param name="flag">String</param>
        private void moving_flag(string flag)
        {
            string retval;
            string value;

            while (true)
            {
                retval = SerialConnection.SendCommand(flag);
                retval = SerialConnection._domeSerial.ReadLine();
                SerialConnection._domeSerial.DiscardInBuffer();
                value = StringToInt(retval);
                unidome_form.textBox_debug_set("moving_flag " + value);
                Thread.Sleep(1000);
                if ((value.Equals("0")) || (value.Equals("1")))
				{
                    if (value.Equals("0"))
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Read the device configuration from the ASCOM Profile store
        /// </summary>
        internal void ReadProfile()
        {
            using (Profile driverProfile = new Profile())
            {
                driverProfile.DeviceType = "Dome";
                tl.Enabled = Convert.ToBoolean(driverProfile.GetValue(driverID,
                traceStateProfileName, string.Empty, traceStateDefault));
                comPort = driverProfile.GetValue(driverID, comPortProfileName,
                string.Empty, comPortDefault);
            }
        }

        /// <summary>
        /// Write the device configuration to the ASCOM Profile store
        /// </summary>
        internal void WriteProfile()
        {
            using (Profile driverProfile = new Profile())
            {
                driverProfile.DeviceType = "Dome";
                driverProfile.WriteValue(driverID, traceStateProfileName, tl.Enabled.ToString());
                driverProfile.WriteValue(driverID, comPortProfileName, comPort.ToString());
            }
        }

        /// <summary>
        /// Log helper function that takes formatted strings and arguments
        /// </summary>
        /// <param name="identifier">Identifier string</param>
        /// <param name="message">Message string</param>
        /// <param name="args">Args parameters</param>
        internal void LogMessage(string identifier, string message, params object[] args)
        {
            var msg = string.Format(message, args);
            tl.LogMessage(identifier, msg);
        }
        #endregion //Private properties and methods
    }
}
