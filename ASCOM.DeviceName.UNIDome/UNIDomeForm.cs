using ASCOM.UNIDome;
using ASCOM.Utilities;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASCOM.UNIDome
{
    /// <summary>
    /// Specific Form class of the dome driver (GUI)
    /// </summary>
    public partial class UNIDomeForm : Form
    {
        /// <value>textBox_init displayed in the init DialogForm</value>
        private System.Windows.Forms.TextBox textBox_init;
        /// <value>myTimer displayed in the init DialogForm</value>
        private System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        /// <value>exitFlag used to stop the timer</value>
        public bool exitFlag = false;

        /// <value>upDown_velocity_az displayed in the CONFIG Form</value>
        private System.Windows.Forms.NumericUpDown upDown_velocity_az;
        /// <value>button OK associated to velocity AZ displayed in CONFIG Form</value>
        public System.Windows.Forms.Button bt_ok_velocity_az;
        /// <value>label_velocity_az displayed in the CONFIG Form</value>
        private System.Windows.Forms.Label label_velocity_az;

        /// <value>upDown_velocity_az displayed in the CONFIG Form</value>
        private System.Windows.Forms.NumericUpDown upDown_velocity_alt;
        /// <value>button OK associated to velocity AZ displayed in CONFIG Form</value>
        public System.Windows.Forms.Button bt_ok_velocity_alt;
        /// <value>label_velocity_az displayed in the CONFIG Form</value>
        private System.Windows.Forms.Label label_velocity_alt;

        /// <value>upDown_alt displayed in the CONFIG Form</value>
        private System.Windows.Forms.NumericUpDown upDown_alt;
        /// <value>button OK associated to ALT displayed in CONFIG Form</value>
        public System.Windows.Forms.Button bt_ok_alt;
        /// <value>label_alt_config displayed in the CONFIG Form</value>
        private System.Windows.Forms.Label label_alt_config;

        /// <value>upDown_az displayed in the CONFIG Form</value>
        private System.Windows.Forms.NumericUpDown upDown_az;
        /// <value>button OK associated to AZ displayed in CONFIG Form</value>
        public System.Windows.Forms.Button bt_ok_az;
        /// <value>label_az_config displayed in the CONFIG Form</value>
        private System.Windows.Forms.Label label_az_config;

        /// <value>upDown_set_park displayed in the CONFIG Form</value>
        private System.Windows.Forms.NumericUpDown upDown_set_park;
        /// <value>button OK associated to SET PARK displayed in CONFIG Form</value>
        public System.Windows.Forms.Button bt_ok_set_park;
        /// <value>label_set_park_config displayed in the CONFIG Form</value>
        private System.Windows.Forms.Label label_set_park_config;

        /// <value>upDown_wind displayed in the CONFIG Form</value>
        private System.Windows.Forms.NumericUpDown upDown_wind;
        /// <value>button OK associated to wind displayed in CONFIG Form</value>
        public System.Windows.Forms.Button bt_ok_wind;
        /// <value>label_wind_config displayed in the CONFIG Form</value>
        private System.Windows.Forms.Label label_wind_config;

        /// <value>upDown_humidity displayed in the CONFIG Form</value>
        private System.Windows.Forms.NumericUpDown upDown_humidity;
        /// <value>button OK associated to humidity displayed in CONFIG Form</value>
        public System.Windows.Forms.Button bt_ok_humidity;
        /// <value>label_humidity_config displayed in the CONFIG Form</value>
        private System.Windows.Forms.Label label_humidity_config;

        /// <value>textBox_log displayed in the LOG Form</value>
        private System.Windows.Forms.TextBox textBox_log;
        /// <value>textBox_back_up displayed in the BACK UP Form</value>
        private System.Windows.Forms.TextBox textBox_back_up;
        /// <value>button SAVE displayed in the BACK UP Form</value>
        private System.Windows.Forms.Button  bt_save_back_up;
        /// <value>button SAVE displayed in the LOG Form</value>
        private System.Windows.Forms.Button  bt_save_log;

        /// <value>init boolean by default to true</value>
        private bool init = true;
        /// <value>path_weather_file string</value>
        private string path_weather_file;
        /// <value>path_config_file string</value>
        private string path_config_file;

        /// <value>booleans set to true when user clicks on buttons</value>
        public bool bool_bt_home_az = false;
        public bool bool_bt_home_alt = false;
        public bool bool_bt_home_shutter = false;
        public bool bool_bt_sync_az = false;
        public bool bool_bt_sync_alt = false;
        public bool bool_bt_sync_shutter = false;
        public bool bool_bt_log = false;
        public bool bool_bt_back_up = false;
        public bool bool_bt_east_abs_az = false;
        public bool bool_bt_east_abs_alt = false;
        public bool bool_bt_west_abs_alt = false;
        public bool bool_bt_east_relat_az = false;
        public bool bool_bt_east_relat_alt = false;
        public bool bool_bt_west_relat_az = false;
        public bool bool_bt_west_relat_alt = false;
        public bool bool_bt_open_shutter = false;
        public bool bool_bt_close_shutter = false;
        public bool bool_bt_echo_on = false;
        public bool bool_bt_echo_off = false;
        public bool bool_bt_park = false;
        public bool bool_bt_set_park = false;
        public bool bool_bt_stop_com = false;
        public bool bool_bt_init_com = false;
        public bool bool_bt_stop_dome = false;
        public bool bool_bt_tracking_on = false;
        public bool bool_bt_tracking_off = false;
        public bool bool_bt_full_home = false;
        public bool bool_bt_display = false;
        public bool bool_bt_ok_velocity_az = false;
        public bool bool_bt_ok_velocity_alt = false;
        public bool bool_bt_ok_alt = false;
        public bool bool_bt_ok_az = false;
        public bool bool_bt_ok_set_park = false;
        public bool bool_bt_ok_wind = false;
        public bool bool_bt_ok_humidity = false;
        public bool bool_bt_motor_activation = false;
        public bool bool_bt_weather_path = false;
        public bool bool_bt_config_path = false;
        public bool bool_bt_stall_flag = false;

        /// <summary>
        /// Update the UTC time every second.
        /// This is the method run when the timer is raised.
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            myTimer.Stop();
            if (exitFlag == false)
            {
                dateTime.Value = DateTime.UtcNow;
                myTimer.Enabled = true;
            }
        }

        /// <summary>
        /// Initialize toolTip for each button of the main form.
        /// </summary>
        private void toolTip_init_buttons()
        {
            System.Windows.Forms.ToolTip ToolTip_motor_activation = new System.Windows.Forms.ToolTip();
            ToolTip_motor_activation.SetToolTip(this.bt_motor_activation, "Motor activation");

            System.Windows.Forms.ToolTip ToolTip_full_home = new System.Windows.Forms.ToolTip();
            ToolTip_full_home.SetToolTip(this.bt_home_full, "Full Home");

            System.Windows.Forms.ToolTip ToolTip_config = new System.Windows.Forms.ToolTip();
            ToolTip_config.SetToolTip(this.bt_config, "Config data");

            System.Windows.Forms.ToolTip ToolTip_display = new System.Windows.Forms.ToolTip();
            ToolTip_display.SetToolTip(this.bt_display, "Display AZ, ALT, Shutter");

            System.Windows.Forms.ToolTip ToolTip_tracking_off = new System.Windows.Forms.ToolTip();
            ToolTip_tracking_off.SetToolTip(this.bt_tracking_off, "Tracking off telescope");

            System.Windows.Forms.ToolTip ToolTip_tracking_on = new System.Windows.Forms.ToolTip();
            ToolTip_tracking_on.SetToolTip(this.bt_tracking_on, "Tracking on telescope");

            System.Windows.Forms.ToolTip ToolTip_log_data = new System.Windows.Forms.ToolTip();
            ToolTip_log_data.SetToolTip(this.bt_log, "LOG data from dome");

            System.Windows.Forms.ToolTip ToolTip_back_up = new System.Windows.Forms.ToolTip();
            ToolTip_back_up.SetToolTip(this.bt_back_up, "BACK UP infos (rain, temperature, power, date...)");

            System.Windows.Forms.ToolTip ToolTip_init_com = new System.Windows.Forms.ToolTip();
            ToolTip_init_com.SetToolTip(this.bt_init_com, "Initialization altitude COM");

            System.Windows.Forms.ToolTip ToolTip_stop_com = new System.Windows.Forms.ToolTip();
            ToolTip_stop_com.SetToolTip(this.bt_stop_com, "STOP altitude COM");

            System.Windows.Forms.ToolTip ToolTip_echo_on = new System.Windows.Forms.ToolTip();
            ToolTip_echo_on.SetToolTip(this.bt_echo_on, "Echo mode ON");

            System.Windows.Forms.ToolTip ToolTip_echo_off = new System.Windows.Forms.ToolTip();
            ToolTip_echo_off.SetToolTip(this.bt_echo_off, "Echo mode OFF");

            System.Windows.Forms.ToolTip ToolTip_stop_dome = new System.Windows.Forms.ToolTip();
            ToolTip_stop_dome.SetToolTip(this.bt_stop_dome, "STOP dome");

            System.Windows.Forms.ToolTip ToolTip_set_park = new System.Windows.Forms.ToolTip();
            ToolTip_set_park.SetToolTip(this.bt_set_park, "Set park position");

            System.Windows.Forms.ToolTip ToolTip_park = new System.Windows.Forms.ToolTip();
            ToolTip_park.SetToolTip(this.bt_park, "Park command");

            System.Windows.Forms.ToolTip ToolTip_open_shutter = new System.Windows.Forms.ToolTip();
            ToolTip_open_shutter.SetToolTip(this.bt_open_shutter, "Open shutter");

            System.Windows.Forms.ToolTip ToolTip_close_shutter = new System.Windows.Forms.ToolTip();
            ToolTip_close_shutter.SetToolTip(this.bt_close_shutter, "Close shutter");

            System.Windows.Forms.ToolTip ToolTip_sync_az = new System.Windows.Forms.ToolTip();
            ToolTip_sync_az.SetToolTip(this.bt_sync_az, "Sync AZ");

            System.Windows.Forms.ToolTip ToolTip_sync_alt = new System.Windows.Forms.ToolTip();
            ToolTip_sync_alt.SetToolTip(this.bt_sync_alt, "Sync ALT");

            System.Windows.Forms.ToolTip ToolTip_sync_shutter = new System.Windows.Forms.ToolTip();
            ToolTip_sync_shutter.SetToolTip(this.bt_sync_shutter, "Sync SHUTTER");

            System.Windows.Forms.ToolTip ToolTip_home_az = new System.Windows.Forms.ToolTip();
            ToolTip_home_az.SetToolTip(this.bt_home_az, "HOME az");

            System.Windows.Forms.ToolTip ToolTip_home_alt = new System.Windows.Forms.ToolTip();
            ToolTip_home_alt.SetToolTip(this.bt_home_alt, "HOME alt");

            System.Windows.Forms.ToolTip ToolTip_home_shutter = new System.Windows.Forms.ToolTip();
            ToolTip_home_shutter.SetToolTip(this.bt_home_shutter, "HOME shutter");

            System.Windows.Forms.ToolTip ToolTip_east_abs_alt = new System.Windows.Forms.ToolTip();
            ToolTip_east_abs_alt.SetToolTip(this.bt_east_abs_alt, "EAST absolute altitude movement");

            System.Windows.Forms.ToolTip ToolTip_west_abs_alt = new System.Windows.Forms.ToolTip();
            ToolTip_west_abs_alt.SetToolTip(this.bt_west_abs_alt, "WEST absolute altitude movement");

            System.Windows.Forms.ToolTip ToolTip_east_relat_alt = new System.Windows.Forms.ToolTip();
            ToolTip_east_relat_alt.SetToolTip(this.bt_east_relat_alt, "EAST relative altitude movement");

            System.Windows.Forms.ToolTip ToolTip_west_relat_alt = new System.Windows.Forms.ToolTip();
            ToolTip_west_relat_alt.SetToolTip(this.bt_west_relat_alt, "WEST relative altitude movement");

            System.Windows.Forms.ToolTip ToolTip_east_abs_az = new System.Windows.Forms.ToolTip();
            ToolTip_east_abs_az.SetToolTip(this.bt_east_abs_az, "EAST absolute azimuth movement");

            System.Windows.Forms.ToolTip ToolTip_east_relat_az = new System.Windows.Forms.ToolTip();
            ToolTip_east_relat_az.SetToolTip(this.bt_east_relat_az, "EAST relative azimuth movement");

            System.Windows.Forms.ToolTip ToolTip_west_relat_az = new System.Windows.Forms.ToolTip();
            ToolTip_west_relat_az.SetToolTip(this.bt_west_relat_az, "WEST relative azimuth movement");
        }

        /// <summary>
        /// Constructor of the UNIDomeForm class and initialize
        /// toolTip buttons.
        /// </summary>
        /// <remarks>
        /// Set up the textBox_init to display a Homing message in a
        /// DialogForm before displaying the main Form and starting a timer.
        /// MOTOR ACTIVATION is by default to red to indicate the user
        /// to first click on this button.
        /// </remarks>
        public UNIDomeForm()
        {
            textBox_init = new System.Windows.Forms.TextBox();
            textBox_init.Location = new System.Drawing.Point(12, 9);
            textBox_init.Multiline = true;
            textBox_init.Text = "Fisrt click on the MOTOR ACTIVATION button to use the dome.\r\n" + 
                "Then, click on FULL HOME to apply homing commands on AZ, ALT, SHUTTER\r\n";
            textBox_init.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            textBox_init.Size = new System.Drawing.Size(200, 200);
            textBox_init.ReadOnly = true;
            textBox_init.HideSelection = false;

            var dialog_form = new Form();
            dialog_form.Text = "INIT";
            dialog_form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            dialog_form.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dialog_form.Controls.Add(this.textBox_init);
            dialog_form.ShowDialog();

            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            InitializeComponent();
            this.Show();

            myTimer.Tick += new EventHandler(TimerEventProcessor);
            myTimer.Interval = 1000;
            myTimer.Enabled = true;
            myTimer.Start();
            toolTip_init_buttons();
            label_weather_conditions.Font = new Font("Arial", 20);
            bt_motor_activation.BackColor = Color.Red;
            bt_ok_velocity_az = new System.Windows.Forms.Button();
            bt_ok_velocity_alt = new System.Windows.Forms.Button();
            bt_ok_alt = new System.Windows.Forms.Button();
            bt_ok_az = new System.Windows.Forms.Button();
            bt_ok_set_park = new System.Windows.Forms.Button();
            bt_ok_wind = new System.Windows.Forms.Button();
            bt_ok_humidity = new System.Windows.Forms.Button();
        }

        private void label_az_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void UNIDomeForm_Load(object sender, EventArgs e)
        {

        }

        private void label_cod_shutter_Click(object sender, EventArgs e)
        {

        }

        private void label_pluie_Click(object sender, EventArgs e)
        {

        }

        private void textBox_debug_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// LOG Button opens a new Form to display LOG data and save them
        /// </summary>
        /// <remarks>
        /// Set up the textBox_log to display the LOG data and a button
        /// bt_save_log to save the current status flags into a file
        /// </remarks>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void button1_Click_1(object sender, EventArgs e)
        {
            bool_bt_log = true;

            textBox_log = new System.Windows.Forms.TextBox();
            textBox_log.Location = new System.Drawing.Point(12, 9);
            textBox_log.Multiline = true;
            textBox_log.Text = "";
            textBox_log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            textBox_log.Size = new System.Drawing.Size(200, 200);
            textBox_log.ReadOnly = true;
            textBox_log.HideSelection = false;

            bt_save_log = new System.Windows.Forms.Button();
            bt_save_log.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom |
            System.Windows.Forms.AnchorStyles.Right)));
            bt_save_log.Location = new System.Drawing.Point(215, 215);
            bt_save_log.Size = new System.Drawing.Size(59, 30);
            bt_save_log.Text = "SAVE";
            bt_save_log.UseVisualStyleBackColor = true;

            var log_form = new Form();
            log_form.Text = "LOG";
            log_form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            log_form.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            log_form.Controls.Add(this.textBox_log);
            log_form.Controls.Add(this.bt_save_log);
            log_form.Show();
        }

        /// <summary>
        /// BACK UP Button opens a new Form to display back up data and save them
        /// </summary>
        /// <remarks>
        /// Set up the textBox_back_up to display the BACK UP infos and a button
        /// bt_save_back_up to save the current infos (rain, power, date...)
        /// </remarks>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_save_Click(object sender, EventArgs e)
        {
            bool_bt_back_up = true;

            textBox_back_up = new System.Windows.Forms.TextBox();
            textBox_back_up.Location = new System.Drawing.Point(12, 9);
            textBox_back_up.Multiline = true;
            textBox_back_up.Text = "Infos to save\r\n";
            textBox_back_up.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            textBox_back_up.Size = new System.Drawing.Size(200, 200);
            textBox_back_up.ReadOnly = true;
            textBox_back_up.HideSelection = false;

            bt_save_back_up = new System.Windows.Forms.Button();
            bt_save_back_up.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom |
            System.Windows.Forms.AnchorStyles.Right)));
            bt_save_back_up.Location = new System.Drawing.Point(215, 215);
            bt_save_back_up.Size = new System.Drawing.Size(59, 30);
            bt_save_back_up.Text = "SAVE";
            bt_save_back_up.UseVisualStyleBackColor = true;

            var back_up_form = new Form();
            back_up_form.Text = "BACK UP";
            back_up_form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            back_up_form.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            back_up_form.Controls.Add(this.textBox_back_up);
            back_up_form.Controls.Add(this.bt_save_back_up);
            back_up_form.Show();
        }

        /// <summary>
        /// Initialize openFileDialog_weather and get the 
        /// filename choosen by the user (txt file by fefault)
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_weather_path_Click(object sender, EventArgs e)
        {
            openFileDialog_weather.InitialDirectory = @"C:\Users\OGS\Documents\";
            openFileDialog_weather.DefaultExt = "txt";
            openFileDialog_weather.Filter = "txt files (*.txt)|*.txt";
            openFileDialog_weather.FilterIndex = 2;
            openFileDialog_weather.CheckFileExists = true;
            openFileDialog_weather.CheckPathExists = true;
            openFileDialog_weather.ShowDialog();
            path_weather_file = openFileDialog_weather.FileName;
            bool_bt_weather_path = true;
        }

        /// <summary>
        /// Initialize openFileDialog_config and get the 
        /// filename choosen by the user (txt file by fefault)
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_config_path_Click(object sender, EventArgs e)
        {
            openFileDialog_config.InitialDirectory = @"C:\Users\OGS\Documents\";
            openFileDialog_config.DefaultExt = "txt";
            openFileDialog_config.Filter = "txt files (*.txt)|*.txt";
            openFileDialog_config.FilterIndex = 2;
            openFileDialog_config.CheckFileExists = true;
            openFileDialog_config.CheckPathExists = true;
            openFileDialog_config.ShowDialog();
            path_config_file = openFileDialog_config.FileName;
            bool_bt_config_path = true;
        }

        /// <summary>
        /// CONFIG Button opens a new Form to ask the user to specify
        /// ALT/AZ offset, the velocity, park position, wind
        /// threshold and humidity threshold.
        /// Uses upDown and OK buttons to validate the choice.
        /// Manage also EventHandler for OK buttons in CONFIG form.
        /// </summary>
        /// <remarks>
        /// The velocity is between 0 and 10. The AZ offset is
        /// between 0 and 360 and the ALT offset between 0 and 90.
        /// The park position is between 0 and 360. The wind is in 
        /// km/h and humididity is in %.
        /// </remarks>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_config_Click(object sender, EventArgs e)
        {
            upDown_velocity_az = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_velocity_az)).BeginInit();
            upDown_velocity_az.Increment = new decimal(new int[] {1, 0, 0, 0});
            upDown_velocity_az.Location = new System.Drawing.Point(95, 20);
            upDown_velocity_az.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            upDown_velocity_az.Maximum = new decimal(new int[] {10, 0, 0, 0 });
            upDown_velocity_az.Name = "upDown_velocity_az";
            upDown_velocity_az.Size = new System.Drawing.Size(88, 22);
            upDown_velocity_az.Value = new decimal(new int[] {5, 0, 0, 0});

            upDown_velocity_alt = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_velocity_alt)).BeginInit();
            upDown_velocity_alt.Increment = new decimal(new int[] { 1, 0, 0, 0 });
            upDown_velocity_alt.Location = new System.Drawing.Point(95, 50);
            upDown_velocity_alt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            upDown_velocity_alt.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            upDown_velocity_alt.Name = "upDown_velocity_alt";
            upDown_velocity_alt.Size = new System.Drawing.Size(88, 22);
            upDown_velocity_alt.Value = new decimal(new int[] { 5, 0, 0, 0 });

            upDown_alt = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_alt)).BeginInit();
            upDown_alt.Increment = new decimal(new int[] { 1, 0, 0, 0 });
            upDown_alt.Location = new System.Drawing.Point(95, 80);
            upDown_alt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            upDown_alt.Maximum = new decimal(new int[] { 90, 0, 0, 0 });
            upDown_alt.Name = "upDown_alt";
            upDown_alt.Size = new System.Drawing.Size(88, 22);
            upDown_alt.Value = new decimal(new int[] { 0, 0, 0, 0 });

            upDown_az = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_az)).BeginInit();
            upDown_az.Increment = new decimal(new int[] { 1, 0, 0, 0 });
            upDown_az.Location = new System.Drawing.Point(95,110);
            upDown_az.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            upDown_az.Maximum = new decimal(new int[] { 360, 0, 0, 0 });
            upDown_az.Name = "upDown_az";
            upDown_az.Size = new System.Drawing.Size(88, 22);
            upDown_az.Value = new decimal(new int[] { 0, 0, 0, 0 });

            upDown_set_park = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_set_park)).BeginInit();
            upDown_set_park.Increment = new decimal(new int[] { 1, 0, 0, 0 });
            upDown_set_park.Location = new System.Drawing.Point(95, 140);
            upDown_set_park.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            upDown_set_park.Maximum = new decimal(new int[] { 360, 0, 0, 0 });
            upDown_set_park.Name = "upDown_set_park";
            upDown_set_park.Size = new System.Drawing.Size(88, 22);
            upDown_set_park.Value = new decimal(new int[] { 180, 0, 0, 0 });

            upDown_wind = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_wind)).BeginInit();
            upDown_wind.Increment = new decimal(new int[] { 1, 0, 0, 0 });
            upDown_wind.Location = new System.Drawing.Point(95, 170);
            upDown_wind.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            upDown_wind.Maximum = new decimal(new int[] { 100, 0, 0, 0 });
            upDown_wind.Name = "upDown_wind";
            upDown_wind.Size = new System.Drawing.Size(88, 22);
            upDown_wind.Value = new decimal(new int[] { 40, 0, 0, 0 });

            upDown_humidity = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_humidity)).BeginInit();
            upDown_humidity.Increment = new decimal(new int[] { 1, 0, 0, 0 });
            upDown_humidity.Location = new System.Drawing.Point(95, 200);
            upDown_humidity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            upDown_humidity.Maximum = new decimal(new int[] { 100, 0, 0, 0 });
            upDown_humidity.Name = "upDown_humidity";
            upDown_humidity.Size = new System.Drawing.Size(88, 22);
            upDown_humidity.Value = new decimal(new int[] { 80, 0, 0, 0 });
            
            bt_ok_velocity_az.Location = new System.Drawing.Point(200, 16);
            bt_ok_velocity_az.Size = new System.Drawing.Size(59, 30);
            bt_ok_velocity_az.Text = "OK";
            bt_ok_velocity_az.UseVisualStyleBackColor = true;
            
            bt_ok_velocity_alt.Location = new System.Drawing.Point(200, 46);
            bt_ok_velocity_alt.Size = new System.Drawing.Size(59, 30);
            bt_ok_velocity_alt.Text = "OK";
            bt_ok_velocity_alt.UseVisualStyleBackColor = true;

            bt_ok_alt.Location = new System.Drawing.Point(200, 76);
            bt_ok_alt.Size = new System.Drawing.Size(59, 30);
            bt_ok_alt.Text = "OK";
            bt_ok_alt.UseVisualStyleBackColor = true;

            bt_ok_az.Location = new System.Drawing.Point(200, 106);
            bt_ok_az.Size = new System.Drawing.Size(59, 30);
            bt_ok_az.Text = "OK";
            bt_ok_az.UseVisualStyleBackColor = true;

            bt_ok_set_park.Location = new System.Drawing.Point(200, 136);
            bt_ok_set_park.Size = new System.Drawing.Size(59, 30);
            bt_ok_set_park.Text = "OK";
            bt_ok_set_park.UseVisualStyleBackColor = true;

            bt_ok_wind.Location = new System.Drawing.Point(200, 166);
            bt_ok_wind.Size = new System.Drawing.Size(59, 30);
            bt_ok_wind.Text = "OK";
            bt_ok_wind.UseVisualStyleBackColor = true;

            bt_ok_humidity.Location = new System.Drawing.Point(200, 196);
            bt_ok_humidity.Size = new System.Drawing.Size(59, 30);
            bt_ok_humidity.Text = "OK";
            bt_ok_humidity.UseVisualStyleBackColor = true;

            label_velocity_az = new System.Windows.Forms.Label();
            label_velocity_az.AutoSize = true;
            label_velocity_az.Location = new System.Drawing.Point(12, 22);
            label_velocity_az.Name = "label_velocity_az";
            label_velocity_az.Size = new System.Drawing.Size(16, 17);
            label_velocity_az.Text = "Velocity AZ";

            label_velocity_alt = new System.Windows.Forms.Label();
            label_velocity_alt.AutoSize = true;
            label_velocity_alt.Location = new System.Drawing.Point(12, 52);
            label_velocity_alt.Name = "label_velocity_alt";
            label_velocity_alt.Size = new System.Drawing.Size(16, 17);
            label_velocity_alt.Text = "Velocity ALT";

            label_alt_config = new System.Windows.Forms.Label();
            label_alt_config.AutoSize = true;
            label_alt_config.Location = new System.Drawing.Point(12, 82);
            label_alt_config.Name = "label_alt_config";
            label_alt_config.Size = new System.Drawing.Size(16, 17);
            label_alt_config.Text = "ALT offset";

            label_az_config = new System.Windows.Forms.Label();
            label_az_config.AutoSize = true;
            label_az_config.Location = new System.Drawing.Point(12, 112);
            label_az_config.Name = "label_az_config";
            label_az_config.Size = new System.Drawing.Size(16, 17);
            label_az_config.Text = "AZ offset";

            label_set_park_config = new System.Windows.Forms.Label();
            label_set_park_config.AutoSize = true;
            label_set_park_config.Location = new System.Drawing.Point(12, 142);
            label_set_park_config.Name = "label_set_park_config";
            label_set_park_config.Size = new System.Drawing.Size(16, 17);
            label_set_park_config.Text = "Set Park";

            label_wind_config = new System.Windows.Forms.Label();
            label_wind_config.AutoSize = true;
            label_wind_config.Location = new System.Drawing.Point(12, 172);
            label_wind_config.Name = "label_wind_config";
            label_wind_config.Size = new System.Drawing.Size(16, 17);
            label_wind_config.Text = "Wind km/h";

            label_humidity_config = new System.Windows.Forms.Label();
            label_humidity_config.AutoSize = true;
            label_humidity_config.Location = new System.Drawing.Point(12, 202);
            label_humidity_config.Name = "label_humidity_config";
            label_humidity_config.Size = new System.Drawing.Size(16, 17);
            label_humidity_config.Text = "Humidity %";

            var config_form = new Form();
            config_form.Size = new Size(400, 400);
            config_form.Text = "CONFIG";
            config_form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            config_form.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            config_form.Controls.Add(this.upDown_velocity_az);
            config_form.Controls.Add(this.upDown_velocity_alt);
            config_form.Controls.Add(this.upDown_alt);
            config_form.Controls.Add(this.upDown_az);
            config_form.Controls.Add(this.upDown_set_park);
            config_form.Controls.Add(this.upDown_wind);
            config_form.Controls.Add(this.upDown_humidity);
            config_form.Controls.Add(this.bt_ok_velocity_az);
            config_form.Controls.Add(this.bt_ok_velocity_alt);
            config_form.Controls.Add(this.bt_ok_alt);
            config_form.Controls.Add(this.bt_ok_az);
            config_form.Controls.Add(this.bt_ok_set_park);
            config_form.Controls.Add(this.bt_ok_wind);
            config_form.Controls.Add(this.bt_ok_humidity);
            config_form.Controls.Add(this.label_velocity_az);
            config_form.Controls.Add(this.label_velocity_alt);
            config_form.Controls.Add(this.label_alt_config);
            config_form.Controls.Add(this.label_az_config);
            config_form.Controls.Add(this.label_set_park_config);
            config_form.Controls.Add(this.label_wind_config);
            config_form.Controls.Add(this.label_humidity_config);
            config_form.Show();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_velocity_az)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_velocity_alt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_alt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_az)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_set_park)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_wind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_humidity)).EndInit();

            bt_ok_velocity_az.Click += new System.EventHandler(this.bt_ok_velocity_az_Click);
            bt_ok_velocity_alt.Click += new System.EventHandler(this.bt_ok_velocity_alt_Click);
            bt_ok_alt.Click += new System.EventHandler(this.bt_ok_alt_Click);
            bt_ok_az.Click += new System.EventHandler(this.bt_ok_az_Click);
            bt_ok_set_park.Click += new System.EventHandler(this.bt_ok_set_park_Click);
            bt_ok_wind.Click += new System.EventHandler(this.bt_ok_wind_Click);
            bt_ok_humidity.Click += new System.EventHandler(this.bt_ok_humidity_Click);
        }

        private void label_stall_shutter_Click(object sender, EventArgs e)
        {

        }

        private void label_alt_Click(object sender, EventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label_shutter_Click(object sender, EventArgs e)
        {

        }

        private void upDown_az_abs_ValueChanged(object sender, EventArgs e)
        {

        }

        private void upDown_alt_abs_ValueChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        public void bt_ok_velocity_az_Click(object sender, EventArgs e)
        {
            bool_bt_ok_velocity_az = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        public void bt_ok_velocity_alt_Click(object sender, EventArgs e)
        {
            bool_bt_ok_velocity_alt = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        public void bt_ok_alt_Click(object sender, EventArgs e)
        {
            bool_bt_ok_alt = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        public void bt_ok_az_Click(object sender, EventArgs e)
        {
            bool_bt_ok_az = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        public void bt_ok_set_park_Click(object sender, EventArgs e)
        {
            bool_bt_ok_set_park = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        public void bt_ok_wind_Click(object sender, EventArgs e)
        {
            bool_bt_ok_wind = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        public void bt_ok_humidity_Click(object sender, EventArgs e)
        {
            bool_bt_ok_humidity = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_sync_az_Click(object sender, EventArgs e)
        {
            bool_bt_sync_az = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_sync_alt_Click(object sender, EventArgs e)
        {
            bool_bt_sync_alt = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_sync_shutter_Click(object sender, EventArgs e)
        {
            bool_bt_sync_shutter = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void button1_Click(object sender, EventArgs e)
        {
            bool_bt_home_az = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void button3_Click(object sender, EventArgs e)
        {
            bool_bt_home_shutter = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_open_shutter_Click(object sender, EventArgs e)
        {
            bool_bt_open_shutter = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_close_shutter_Click(object sender, EventArgs e)
        {
            bool_bt_close_shutter = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_home_alt_Click(object sender, EventArgs e)
        {
            bool_bt_home_alt = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_east_abs_alt_Click(object sender, EventArgs e)
        {
            bool_bt_east_abs_alt = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_west_abs_alt_Click(object sender, EventArgs e)
        {
            bool_bt_west_abs_alt = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_east_relat_alt_Click(object sender, EventArgs e)
        {
            bool_bt_east_relat_alt = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_west_relat_alt_Click(object sender, EventArgs e)
        {
            bool_bt_west_relat_alt = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_east_abs_az_Click(object sender, EventArgs e)
        {
            bool_bt_east_abs_az = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_east_relat_az_Click(object sender, EventArgs e)
        {
            bool_bt_east_relat_az = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_west_relat_az_Click(object sender, EventArgs e)
        {
            bool_bt_west_relat_az = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_echo_on_Click(object sender, EventArgs e)
        {
            bool_bt_echo_on = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_echo_off_Click(object sender, EventArgs e)
        {
            bool_bt_echo_off = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_park_Click(object sender, EventArgs e)
        {
            bool_bt_park = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_set_park_Click(object sender, EventArgs e)
        {
            bool_bt_set_park = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_stop_com_Click(object sender, EventArgs e)
        {
            bool_bt_stop_com = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_init_com_Click(object sender, EventArgs e)
        {
            bool_bt_init_com = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_stop_dome_Click(object sender, EventArgs e)
        {
            bool_bt_stop_dome = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_tracking_Click(object sender, EventArgs e)
        {
            bool_bt_tracking_on = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_tracking_off_Click(object sender, EventArgs e)
        {
            bool_bt_tracking_off = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_display_Click(object sender, EventArgs e)
        {
            bool_bt_display = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_stall_flag_Click(object sender, EventArgs e)
        {
            bool_bt_stall_flag = true;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button.
        /// The button is set to green when the user click on it.
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt_home_full_Click(object sender, EventArgs e)
        {
            bool_bt_full_home = true;
            bt_home_full.BackColor = Color.Green;
        }

        /// <summary>
        /// Boolean set to true when the user clicks on the button.
        /// The button is set to green when the user click on it.
        /// Then, the FULL HOME button is set to red to indicate
        /// to the user to click on it before doing anything else.
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">EventArgs</param>
        private void bt__force_open_Click(object sender, EventArgs e)
        {
            bool_bt_motor_activation = true;
            bt_motor_activation.BackColor = Color.Green;
            if (init == true)
            {
                bt_home_full.BackColor = Color.Red;
                init = false;
            }
                
        }

        /// <summary>
        /// Set the bad weather conditions warning:
        /// main form backgrounnd set to orange,
        /// change text label and font.
        /// </summary>
        public void bad_weather_condition_warning()
        {
            label_weather_conditions.Text = "BAD WEATHER CONDITIONS !!!";
            label_weather_conditions.Font = new Font("Arial", 20, FontStyle.Bold);
            this.BackColor = Color.DarkOrange;
        }

        /// <summary>
        /// Setter of the log textbox
        /// </summary>
        /// <param name="s">String</param>
        public void textBox_log_set(string s)
        {
            textBox_log.Text = textBox_log.Text + s;
        }

        /// <summary>
        /// Setter of the debug textBox. Put new data in the
        /// top of textBox to get an auto scrolling feature.
        /// </summary>
        /// <param name="s">String</param>
        public void textBox_debug_set(string s)
        {
            textBox_debug.Text = s + " |\r\n" + textBox_debug.Text;
        }

        /// <summary>
        /// Setter of the altitude label
        /// </summary>
        /// <param name="s">String</param>
        public void label_alt_set(string s)
        {
            label_alt.Text = s;
        }

        /// <summary>
        /// Setter of the azimuth label
        /// </summary>
        /// <param name="s">String</param>
        public void label_az_set(string s)
        {
            label_az.Text = s;
        }

        /// <summary>
        /// Setter of the shutter label
        /// </summary>
        /// <param name="s">String</param>
        public void label_shutter_set(string s)
        {
            label_shutter.Text = s;
        }

        /// <summary>
        /// Setter of the az coder label
        /// </summary>
        /// <param name="s">String</param>
        public void label_cod_az_set(string s)
        {
            label_cod_az.Text = s;
        }

        /// <summary>
        /// Setter of the alt coder label
        /// </summary>
        /// <param name="s">String</param>
        public void label_cod_alt_set(string s)
        {
            label_cod_alt.Text = s;
        }

        /// <summary>
        /// Setter of the shutter coder label
        /// </summary>
        /// <param name="s">String</param>
        public void label_cod_shutter_set(string s)
        {
            label_cod_shutter.Text = s;
        }

        /// <summary>
        /// Setter of the az stall label
        /// </summary>
        /// <param name="s">String</param>
        public void label_stall_az_set(string s)
        {
            label_stall_az.Text = s;
        }

        /// <summary>
        /// Setter of the alt stall label
        /// </summary>
        /// <param name="s">String</param>
        public void label_stall_alt_set(string s)
        {
            label_stall_alt.Text = s;
        }

        /// <summary>
        /// Setter of the shutter stall label
        /// </summary>
        /// <param name="s">String</param>
        public void label_stall_shutter_set(string s)
        {
            label_stall_shutter.Text = s;
        }

        /// <summary>
        /// Setter of the wind label
        /// </summary>
        /// <param name="s">String</param>
        public void label_wind_set(string s)
        {
            label_wind.Text = "wind: " + s + "km/h";
        }

        /// <summary>
        /// Setter of the wind threshold label
        /// </summary>
        /// <param name="s">String</param>
        public void label_wind_threshold_set(string s)
        {
            label_wind_threshold.Text = "wind threshold: " + s + "km/h";
        }

        /// <summary>
        /// Setter of the humidity label
        /// </summary>
        /// <param name="s">String</param>
        public void label_humidity_set(string s)
        {
            label_humidity.Text = "humidity: " + s + "%";
        }

        /// <summary>
        /// Setter of the humidity threshold label
        /// </summary>
        /// <param name="s">String</param>
        public void label_humidity_threshold_set(string s)
        {
            label_humidity_threshold.Text = "humidity threshold: " + s + "%";
        }

        /// <summary>
        /// Setter of the AZ telescope label
        /// </summary>
        /// <param name="s">String</param>
        public void label_az_telescope_set(string s)
        {
            label_az_telescope.Text = "AZ telescope: " + s;
        }

        /// <summary>
        /// Setter of the ALT telescope label
        /// </summary>
        /// <param name="s">String</param>
        public void label_alt_telescope_set(string s)
        {
            label_alt_telescope.Text = "ALT telescope: " + s;
        }

        /// <summary>
        /// Setter of the temperature in label
        /// </summary>
        /// <param name="s">String</param>
        public void label_temperature_in_set(string s)
        {
            label_temperature_in.Text = "temperature in: " + s + "�C";
        }

        /// <summary>
        /// Setter of the temperature out label
        /// </summary>
        /// <param name="s">String</param>
        public void label_temperature_out_set(string s)
        {
            label_temperature_out.Text = "temperature out: " + s + "�C";
        }

        /// <summary>
        /// Setter of the dew point in label
        /// </summary>
        /// <param name="s">String</param>
        public void label_dew_point_in_set(string s)
        {
            label_dew_point_in.Text = "dew point in: " + s + "�C";
        }

        /// <summary>
        /// Setter of the dew point out label
        /// </summary>
        /// <param name="s">String</param>
        public void label_dew_point_out_set(string s)
        {
            label_dew_point_out.Text = "dew point out: " + s + "�C";
        }

        /// <summary>
        /// Setter of the humidity in label
        /// </summary>
        /// <param name="s">String</param>
        public void label_humidity_in_set(string s)
        {
            label_humidity_in.Text = "humidity in: " + s + "%";
        }

        /// <summary>
        /// Setter of the humidity out label
        /// </summary>
        /// <param name="s">String</param>
        public void label_humidity_out_set(string s)
        {
            label_humidity_out.Text = "humidity out: " + s + "%";
        }

        /// <summary>
        /// Getter of the abs azimuth upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_abs_az_get()
        {
            return Convert.ToInt32(Math.Round(upDown_az_abs.Value, 0));
        }

        /// <summary>
        /// Getter of the relative azimuth upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_relat_az_get()
        {
            return Convert.ToInt32(Math.Round(upDown_az_relat.Value, 0));
        }

        /// <summary>
        /// Getter of the abs altitude upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_abs_alt_get()
        {
            return Convert.ToInt32(Math.Round(upDown_alt_abs.Value, 0));
        }

        /// <summary>
        /// Getter of the relat altitude upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_relat_alt_get()
        {
            return Convert.ToInt32(Math.Round(upDown_alt_relat.Value, 0));
        }

        /// <summary>
        /// Getter of the shutter upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_shutter_get()
        {
            return Convert.ToInt32(Math.Round(upDown_shutter.Value, 0));
        }

        /// <summary>
        /// Getter of the sync az upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_sync_az_get()
        {
            return Convert.ToInt32(Math.Round(upDown_sync_az.Value, 0));
        }

        /// <summary>
        /// Getter of the sync alt upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_sync_alt_get()
        {
            return Convert.ToInt32(Math.Round(upDown_sync_alt.Value, 0));
        }

        /// <summary>
        /// Getter of the sync shutter upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_sync_shutter_get()
        {
            return Convert.ToInt32(Math.Round(upDown_sync_shutter.Value, 0));
        }

        /// <summary>
        /// Getter of the velocity AZ upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_velocity_az_get()
        {
            return Convert.ToInt32(Math.Round(upDown_velocity_az.Value, 0));
        }

        /// <summary>
        /// Getter of the velocity ALT upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_velocity_alt_get()
        {
            return Convert.ToInt32(Math.Round(upDown_velocity_alt.Value, 0));
        }

        /// <summary>
        /// Getter of the ALT offset upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_alt_offset_get()
        {
            return Convert.ToInt32(Math.Round(upDown_alt.Value, 0));
        }

        /// <summary>
        /// Getter of the AZ offset upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_az_offset_get()
        {
            return Convert.ToInt32(Math.Round(upDown_az.Value, 0));
        }

        /// <summary>
        /// Getter of the set park upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_set_park_get()
        {
            return Convert.ToInt32(Math.Round(upDown_set_park.Value, 0));
        }

        /// <summary>
        /// Getter of the wind upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_wind_get()
        {
            return Convert.ToInt32(Math.Round(upDown_wind.Value, 0));
        }

        /// <summary>
        /// Getter of the humidity upDown
        /// </summary>
        /// <returns>
        /// int value
        /// </returns>
        public int upDown_humidity_get()
        {
            return Convert.ToInt32(Math.Round(upDown_humidity.Value, 0));
        }

        /// <summary>
        /// Getter of the weather file path
        /// </summary>
        /// <returns>
        /// string value
        /// </returns>
        public string weather_file_path_get()
        {
            return path_weather_file;
        }

        /// <summary>
        /// Getter of the config data file path
        /// </summary>
        /// <returns>
        /// string value
        /// </returns>
        public string config_file_path_get()
        {
            return path_config_file;
        }

        /// <summary>
        /// Draw a red box when the dome is moving
        /// </summary>
        public void draw_red_box()
        {
            Graphics g = this.CreateGraphics();
            g.FillRectangle(Brushes.Red, 70, 390, 25, 25);
        }

        /// <summary>
        /// Draw a green box when the dome is not moving
        /// </summary>
        public void draw_green_box()
        {
            Graphics g = this.CreateGraphics();
            g.FillRectangle(Brushes.Green, 70, 390, 25, 25);
        }

        /// <summary>
        /// Draw a colored box in function of rain information:
        /// green if 0, orange if 1 and red if 2
        /// </summary>
        /// <param name="value">Int value</param>
        public void draw_box_rain(int value)
        {
            if (value == 0)
                label_color_rain.BackColor = Color.Green;
            else if (value == 1)
                label_color_rain.BackColor = Color.Orange;
            else if (value == 2)
                label_color_rain.BackColor = Color.Red;
        }

        /// <summary>
        /// Draw a colored box in function of cloud information:
        /// black if 0 unknown, yellow if 1 clear, orange if 2 cloudy, 
        /// and red if 3 very cloudy
        /// </summary>
        /// <param name="value">Int value</param>
        public void draw_box_cloud(int value)
        {
            if (value == 0)
                label_color_cloud.BackColor = Color.Black;
            else if (value == 1)
                label_color_cloud.BackColor = Color.Green;
            else if (value == 2)
                label_color_cloud.BackColor = Color.Orange;
            else if (value == 3)
                label_color_cloud.BackColor = Color.Red;
        }
    }
}
