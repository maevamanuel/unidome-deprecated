
namespace ASCOM.UNIDome
{
    partial class UNIDomeForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UNIDomeForm));
            this.bt_home_az = new System.Windows.Forms.Button();
            this.bt_home_alt = new System.Windows.Forms.Button();
            this.bt_home_shutter = new System.Windows.Forms.Button();
            this.label_az = new System.Windows.Forms.Label();
            this.label_alt = new System.Windows.Forms.Label();
            this.label_cod_az = new System.Windows.Forms.Label();
            this.label_cod_alt = new System.Windows.Forms.Label();
            this.label_cod_shutter = new System.Windows.Forms.Label();
            this.upDown_az_abs = new System.Windows.Forms.NumericUpDown();
            this.upDown_alt_abs = new System.Windows.Forms.NumericUpDown();
            this.bt_sync_az = new System.Windows.Forms.Button();
            this.bt_sync_alt = new System.Windows.Forms.Button();
            this.bt_log = new System.Windows.Forms.Button();
            this.bt_back_up = new System.Windows.Forms.Button();
            this.dateTime = new System.Windows.Forms.DateTimePicker();
            this.label_stall_az = new System.Windows.Forms.Label();
            this.label_stall_alt = new System.Windows.Forms.Label();
            this.label_stall_shutter = new System.Windows.Forms.Label();
            this.upDown_az_relat = new System.Windows.Forms.NumericUpDown();
            this.upDown_alt_relat = new System.Windows.Forms.NumericUpDown();
            this.label_abs_upDown = new System.Windows.Forms.Label();
            this.label_relat_upDown = new System.Windows.Forms.Label();
            this.bt_east_abs_az = new System.Windows.Forms.Button();
            this.bt_east_abs_alt = new System.Windows.Forms.Button();
            this.bt_west_abs_alt = new System.Windows.Forms.Button();
            this.bt_east_relat_az = new System.Windows.Forms.Button();
            this.bt_west_relat_az = new System.Windows.Forms.Button();
            this.bt_east_relat_alt = new System.Windows.Forms.Button();
            this.bt_west_relat_alt = new System.Windows.Forms.Button();
            this.bt_open_shutter = new System.Windows.Forms.Button();
            this.bt_close_shutter = new System.Windows.Forms.Button();
            this.upDown_shutter = new System.Windows.Forms.NumericUpDown();
            this.bt_sync_shutter = new System.Windows.Forms.Button();
            this.label_shutter = new System.Windows.Forms.Label();
            this.bt_echo_on = new System.Windows.Forms.Button();
            this.bt_echo_off = new System.Windows.Forms.Button();
            this.bt_park = new System.Windows.Forms.Button();
            this.bt_set_park = new System.Windows.Forms.Button();
            this.upDown_sync_az = new System.Windows.Forms.NumericUpDown();
            this.upDown_sync_alt = new System.Windows.Forms.NumericUpDown();
            this.upDown_sync_shutter = new System.Windows.Forms.NumericUpDown();
            this.bt_stop_com = new System.Windows.Forms.Button();
            this.bt_stop_dome = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.bt_init_com = new System.Windows.Forms.Button();
            this.bt_tracking_on = new System.Windows.Forms.Button();
            this.bt_home_full = new System.Windows.Forms.Button();
            this.bt_display = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.date_picker = new System.Windows.Forms.DateTimePicker();
            this.bt_config = new System.Windows.Forms.Button();
            this.bt_motor_activation = new System.Windows.Forms.Button();
            this.textBox_debug = new System.Windows.Forms.TextBox();
            this.bt_tracking_off = new System.Windows.Forms.Button();
            this.openFileDialog_weather = new System.Windows.Forms.OpenFileDialog();
            this.bt_weather_path = new System.Windows.Forms.Button();
            this.bt_config_path = new System.Windows.Forms.Button();
            this.openFileDialog_config = new System.Windows.Forms.OpenFileDialog();
            this.label_weather_conditions = new System.Windows.Forms.Label();
            this.bt_stall_flag = new System.Windows.Forms.Button();
            this.label_rain = new System.Windows.Forms.Label();
            this.label_humidity = new System.Windows.Forms.Label();
            this.label_cloud = new System.Windows.Forms.Label();
            this.label_wind = new System.Windows.Forms.Label();
            this.label_az_telescope = new System.Windows.Forms.Label();
            this.label_alt_telescope = new System.Windows.Forms.Label();
            this.label_temperature_in = new System.Windows.Forms.Label();
            this.label_dew_point_in = new System.Windows.Forms.Label();
            this.label_humidity_in = new System.Windows.Forms.Label();
            this.label_temperature_out = new System.Windows.Forms.Label();
            this.label_dew_point_out = new System.Windows.Forms.Label();
            this.label_humidity_out = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label_color_rain = new System.Windows.Forms.Label();
            this.label_color_cloud = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.label_humidity_threshold = new System.Windows.Forms.Label();
            this.label_wind_threshold = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_az_abs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_alt_abs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_az_relat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_alt_relat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_shutter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_sync_az)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_sync_alt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_sync_shutter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // bt_home_az
            // 
            this.bt_home_az.Location = new System.Drawing.Point(96, 220);
            this.bt_home_az.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_home_az.Name = "bt_home_az";
            this.bt_home_az.Size = new System.Drawing.Size(163, 23);
            this.bt_home_az.TabIndex = 0;
            this.bt_home_az.Text = "HOME AZ";
            this.bt_home_az.UseVisualStyleBackColor = true;
            this.bt_home_az.Click += new System.EventHandler(this.button1_Click);
            // 
            // bt_home_alt
            // 
            this.bt_home_alt.Location = new System.Drawing.Point(331, 220);
            this.bt_home_alt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_home_alt.Name = "bt_home_alt";
            this.bt_home_alt.Size = new System.Drawing.Size(175, 23);
            this.bt_home_alt.TabIndex = 1;
            this.bt_home_alt.Text = "HOME ALT";
            this.bt_home_alt.UseVisualStyleBackColor = true;
            this.bt_home_alt.Click += new System.EventHandler(this.bt_home_alt_Click);
            // 
            // bt_home_shutter
            // 
            this.bt_home_shutter.Location = new System.Drawing.Point(585, 220);
            this.bt_home_shutter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_home_shutter.Name = "bt_home_shutter";
            this.bt_home_shutter.Size = new System.Drawing.Size(141, 23);
            this.bt_home_shutter.TabIndex = 2;
            this.bt_home_shutter.Text = "HOME SHUTTER";
            this.bt_home_shutter.UseVisualStyleBackColor = true;
            this.bt_home_shutter.Click += new System.EventHandler(this.button3_Click);
            // 
            // label_az
            // 
            this.label_az.AutoSize = true;
            this.label_az.Location = new System.Drawing.Point(93, 260);
            this.label_az.Name = "label_az";
            this.label_az.Size = new System.Drawing.Size(16, 17);
            this.label_az.TabIndex = 3;
            this.label_az.Text = "0";
            this.label_az.Click += new System.EventHandler(this.label_az_Click);
            // 
            // label_alt
            // 
            this.label_alt.AutoSize = true;
            this.label_alt.Location = new System.Drawing.Point(329, 260);
            this.label_alt.Name = "label_alt";
            this.label_alt.Size = new System.Drawing.Size(16, 17);
            this.label_alt.TabIndex = 4;
            this.label_alt.Text = "0";
            this.label_alt.Click += new System.EventHandler(this.label_alt_Click);
            // 
            // label_cod_az
            // 
            this.label_cod_az.AutoSize = true;
            this.label_cod_az.Location = new System.Drawing.Point(193, 260);
            this.label_cod_az.Name = "label_cod_az";
            this.label_cod_az.Size = new System.Drawing.Size(63, 17);
            this.label_cod_az.TabIndex = 6;
            this.label_cod_az.Text = "coder az";
            this.label_cod_az.Click += new System.EventHandler(this.label1_Click);
            // 
            // label_cod_alt
            // 
            this.label_cod_alt.AutoSize = true;
            this.label_cod_alt.Location = new System.Drawing.Point(441, 260);
            this.label_cod_alt.Name = "label_cod_alt";
            this.label_cod_alt.Size = new System.Drawing.Size(63, 17);
            this.label_cod_alt.TabIndex = 7;
            this.label_cod_alt.Text = "coder alt";
            // 
            // label_cod_shutter
            // 
            this.label_cod_shutter.AutoSize = true;
            this.label_cod_shutter.Location = new System.Drawing.Point(676, 260);
            this.label_cod_shutter.Name = "label_cod_shutter";
            this.label_cod_shutter.Size = new System.Drawing.Size(44, 17);
            this.label_cod_shutter.TabIndex = 8;
            this.label_cod_shutter.Text = "coder";
            this.label_cod_shutter.Click += new System.EventHandler(this.label_cod_shutter_Click);
            // 
            // upDown_az_abs
            // 
            this.upDown_az_abs.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.upDown_az_abs.Location = new System.Drawing.Point(97, 353);
            this.upDown_az_abs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.upDown_az_abs.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.upDown_az_abs.Name = "upDown_az_abs";
            this.upDown_az_abs.Size = new System.Drawing.Size(88, 22);
            this.upDown_az_abs.TabIndex = 9;
            this.upDown_az_abs.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.upDown_az_abs.ValueChanged += new System.EventHandler(this.upDown_az_abs_ValueChanged);
            // 
            // upDown_alt_abs
            // 
            this.upDown_alt_abs.Location = new System.Drawing.Point(332, 353);
            this.upDown_alt_abs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.upDown_alt_abs.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.upDown_alt_abs.Name = "upDown_alt_abs";
            this.upDown_alt_abs.Size = new System.Drawing.Size(96, 22);
            this.upDown_alt_abs.TabIndex = 10;
            this.upDown_alt_abs.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.upDown_alt_abs.ValueChanged += new System.EventHandler(this.upDown_alt_abs_ValueChanged);
            // 
            // bt_sync_az
            // 
            this.bt_sync_az.Location = new System.Drawing.Point(179, 298);
            this.bt_sync_az.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_sync_az.Name = "bt_sync_az";
            this.bt_sync_az.Size = new System.Drawing.Size(80, 30);
            this.bt_sync_az.TabIndex = 11;
            this.bt_sync_az.Text = "SYNC";
            this.bt_sync_az.UseVisualStyleBackColor = true;
            this.bt_sync_az.Click += new System.EventHandler(this.bt_sync_az_Click);
            // 
            // bt_sync_alt
            // 
            this.bt_sync_alt.Location = new System.Drawing.Point(425, 298);
            this.bt_sync_alt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_sync_alt.Name = "bt_sync_alt";
            this.bt_sync_alt.Size = new System.Drawing.Size(83, 30);
            this.bt_sync_alt.TabIndex = 12;
            this.bt_sync_alt.Text = "SYNC";
            this.bt_sync_alt.UseVisualStyleBackColor = true;
            this.bt_sync_alt.Click += new System.EventHandler(this.bt_sync_alt_Click);
            // 
            // bt_log
            // 
            this.bt_log.Location = new System.Drawing.Point(796, 505);
            this.bt_log.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_log.Name = "bt_log";
            this.bt_log.Size = new System.Drawing.Size(141, 28);
            this.bt_log.TabIndex = 15;
            this.bt_log.Text = "LOG DATA";
            this.bt_log.UseVisualStyleBackColor = true;
            this.bt_log.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // bt_back_up
            // 
            this.bt_back_up.Location = new System.Drawing.Point(797, 556);
            this.bt_back_up.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_back_up.Name = "bt_back_up";
            this.bt_back_up.Size = new System.Drawing.Size(140, 26);
            this.bt_back_up.TabIndex = 18;
            this.bt_back_up.Text = "BACK UP";
            this.bt_back_up.UseVisualStyleBackColor = true;
            this.bt_back_up.Click += new System.EventHandler(this.bt_save_Click);
            // 
            // dateTime
            // 
            this.dateTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTime.Location = new System.Drawing.Point(501, 172);
            this.dateTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTime.Name = "dateTime";
            this.dateTime.ShowUpDown = true;
            this.dateTime.Size = new System.Drawing.Size(177, 22);
            this.dateTime.TabIndex = 19;
            this.dateTime.Value = new System.DateTime(2021, 4, 29, 12, 52, 51, 215);
            // 
            // label_stall_az
            // 
            this.label_stall_az.AutoSize = true;
            this.label_stall_az.Location = new System.Drawing.Point(139, 446);
            this.label_stall_az.Name = "label_stall_az";
            this.label_stall_az.Size = new System.Drawing.Size(86, 17);
            this.label_stall_az.TabIndex = 22;
            this.label_stall_az.Text = "stall az false";
            // 
            // label_stall_alt
            // 
            this.label_stall_alt.AutoSize = true;
            this.label_stall_alt.Location = new System.Drawing.Point(376, 446);
            this.label_stall_alt.Name = "label_stall_alt";
            this.label_stall_alt.Size = new System.Drawing.Size(86, 17);
            this.label_stall_alt.TabIndex = 23;
            this.label_stall_alt.Text = "stall alt false";
            // 
            // label_stall_shutter
            // 
            this.label_stall_shutter.AutoSize = true;
            this.label_stall_shutter.Location = new System.Drawing.Point(597, 446);
            this.label_stall_shutter.Name = "label_stall_shutter";
            this.label_stall_shutter.Size = new System.Drawing.Size(115, 17);
            this.label_stall_shutter.TabIndex = 24;
            this.label_stall_shutter.Text = "stall shutter false";
            this.label_stall_shutter.Click += new System.EventHandler(this.label_stall_shutter_Click);
            // 
            // upDown_az_relat
            // 
            this.upDown_az_relat.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.upDown_az_relat.Location = new System.Drawing.Point(97, 401);
            this.upDown_az_relat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.upDown_az_relat.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.upDown_az_relat.Name = "upDown_az_relat";
            this.upDown_az_relat.Size = new System.Drawing.Size(88, 22);
            this.upDown_az_relat.TabIndex = 25;
            this.upDown_az_relat.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // upDown_alt_relat
            // 
            this.upDown_alt_relat.Location = new System.Drawing.Point(332, 401);
            this.upDown_alt_relat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.upDown_alt_relat.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.upDown_alt_relat.Name = "upDown_alt_relat";
            this.upDown_alt_relat.Size = new System.Drawing.Size(96, 22);
            this.upDown_alt_relat.TabIndex = 26;
            this.upDown_alt_relat.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label_abs_upDown
            // 
            this.label_abs_upDown.AutoSize = true;
            this.label_abs_upDown.Location = new System.Drawing.Point(27, 356);
            this.label_abs_upDown.Name = "label_abs_upDown";
            this.label_abs_upDown.Size = new System.Drawing.Size(62, 17);
            this.label_abs_upDown.TabIndex = 27;
            this.label_abs_upDown.Text = "absolute";
            // 
            // label_relat_upDown
            // 
            this.label_relat_upDown.AutoSize = true;
            this.label_relat_upDown.Location = new System.Drawing.Point(29, 402);
            this.label_relat_upDown.Name = "label_relat_upDown";
            this.label_relat_upDown.Size = new System.Drawing.Size(54, 17);
            this.label_relat_upDown.TabIndex = 29;
            this.label_relat_upDown.Text = "relative";
            // 
            // bt_east_abs_az
            // 
            this.bt_east_abs_az.Location = new System.Drawing.Point(199, 352);
            this.bt_east_abs_az.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_east_abs_az.Name = "bt_east_abs_az";
            this.bt_east_abs_az.Size = new System.Drawing.Size(55, 23);
            this.bt_east_abs_az.TabIndex = 31;
            this.bt_east_abs_az.Text = "OK";
            this.bt_east_abs_az.UseVisualStyleBackColor = true;
            this.bt_east_abs_az.Click += new System.EventHandler(this.bt_east_abs_az_Click);
            // 
            // bt_east_abs_alt
            // 
            this.bt_east_abs_alt.Location = new System.Drawing.Point(447, 353);
            this.bt_east_abs_alt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_east_abs_alt.Name = "bt_east_abs_alt";
            this.bt_east_abs_alt.Size = new System.Drawing.Size(25, 23);
            this.bt_east_abs_alt.TabIndex = 33;
            this.bt_east_abs_alt.Text = "+";
            this.bt_east_abs_alt.UseVisualStyleBackColor = true;
            this.bt_east_abs_alt.Click += new System.EventHandler(this.bt_east_abs_alt_Click);
            // 
            // bt_west_abs_alt
            // 
            this.bt_west_abs_alt.Location = new System.Drawing.Point(475, 353);
            this.bt_west_abs_alt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_west_abs_alt.Name = "bt_west_abs_alt";
            this.bt_west_abs_alt.Size = new System.Drawing.Size(32, 23);
            this.bt_west_abs_alt.TabIndex = 34;
            this.bt_west_abs_alt.Text = "-";
            this.bt_west_abs_alt.UseVisualStyleBackColor = true;
            this.bt_west_abs_alt.Click += new System.EventHandler(this.bt_west_abs_alt_Click);
            // 
            // bt_east_relat_az
            // 
            this.bt_east_relat_az.Location = new System.Drawing.Point(195, 401);
            this.bt_east_relat_az.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_east_relat_az.Name = "bt_east_relat_az";
            this.bt_east_relat_az.Size = new System.Drawing.Size(29, 23);
            this.bt_east_relat_az.TabIndex = 35;
            this.bt_east_relat_az.Text = "E";
            this.bt_east_relat_az.UseVisualStyleBackColor = true;
            this.bt_east_relat_az.Click += new System.EventHandler(this.bt_east_relat_az_Click);
            // 
            // bt_west_relat_az
            // 
            this.bt_west_relat_az.Location = new System.Drawing.Point(224, 401);
            this.bt_west_relat_az.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_west_relat_az.Name = "bt_west_relat_az";
            this.bt_west_relat_az.Size = new System.Drawing.Size(35, 23);
            this.bt_west_relat_az.TabIndex = 36;
            this.bt_west_relat_az.Text = "W";
            this.bt_west_relat_az.UseVisualStyleBackColor = true;
            this.bt_west_relat_az.Click += new System.EventHandler(this.bt_west_relat_az_Click);
            // 
            // bt_east_relat_alt
            // 
            this.bt_east_relat_alt.Location = new System.Drawing.Point(447, 400);
            this.bt_east_relat_alt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_east_relat_alt.Name = "bt_east_relat_alt";
            this.bt_east_relat_alt.Size = new System.Drawing.Size(25, 23);
            this.bt_east_relat_alt.TabIndex = 37;
            this.bt_east_relat_alt.Text = "+";
            this.bt_east_relat_alt.UseVisualStyleBackColor = true;
            this.bt_east_relat_alt.Click += new System.EventHandler(this.bt_east_relat_alt_Click);
            // 
            // bt_west_relat_alt
            // 
            this.bt_west_relat_alt.Location = new System.Drawing.Point(475, 401);
            this.bt_west_relat_alt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_west_relat_alt.Name = "bt_west_relat_alt";
            this.bt_west_relat_alt.Size = new System.Drawing.Size(32, 23);
            this.bt_west_relat_alt.TabIndex = 38;
            this.bt_west_relat_alt.Text = "-";
            this.bt_west_relat_alt.UseVisualStyleBackColor = true;
            this.bt_west_relat_alt.Click += new System.EventHandler(this.bt_west_relat_alt_Click);
            // 
            // bt_open_shutter
            // 
            this.bt_open_shutter.Location = new System.Drawing.Point(587, 390);
            this.bt_open_shutter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_open_shutter.Name = "bt_open_shutter";
            this.bt_open_shutter.Size = new System.Drawing.Size(64, 30);
            this.bt_open_shutter.TabIndex = 39;
            this.bt_open_shutter.Text = "OPEN";
            this.bt_open_shutter.UseVisualStyleBackColor = true;
            this.bt_open_shutter.Click += new System.EventHandler(this.bt_open_shutter_Click);
            // 
            // bt_close_shutter
            // 
            this.bt_close_shutter.Location = new System.Drawing.Point(656, 390);
            this.bt_close_shutter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_close_shutter.Name = "bt_close_shutter";
            this.bt_close_shutter.Size = new System.Drawing.Size(68, 30);
            this.bt_close_shutter.TabIndex = 40;
            this.bt_close_shutter.Text = "CLOSE";
            this.bt_close_shutter.UseVisualStyleBackColor = true;
            this.bt_close_shutter.Click += new System.EventHandler(this.bt_close_shutter_Click);
            // 
            // upDown_shutter
            // 
            this.upDown_shutter.Location = new System.Drawing.Point(587, 350);
            this.upDown_shutter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.upDown_shutter.Name = "upDown_shutter";
            this.upDown_shutter.Size = new System.Drawing.Size(141, 22);
            this.upDown_shutter.TabIndex = 41;
            this.upDown_shutter.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // bt_sync_shutter
            // 
            this.bt_sync_shutter.Location = new System.Drawing.Point(665, 298);
            this.bt_sync_shutter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_sync_shutter.Name = "bt_sync_shutter";
            this.bt_sync_shutter.Size = new System.Drawing.Size(61, 30);
            this.bt_sync_shutter.TabIndex = 42;
            this.bt_sync_shutter.Text = "SYNC";
            this.bt_sync_shutter.UseVisualStyleBackColor = true;
            this.bt_sync_shutter.Click += new System.EventHandler(this.bt_sync_shutter_Click);
            // 
            // label_shutter
            // 
            this.label_shutter.AutoSize = true;
            this.label_shutter.Location = new System.Drawing.Point(583, 260);
            this.label_shutter.Name = "label_shutter";
            this.label_shutter.Size = new System.Drawing.Size(49, 17);
            this.label_shutter.TabIndex = 5;
            this.label_shutter.Text = "closed";
            this.label_shutter.Click += new System.EventHandler(this.label_shutter_Click);
            // 
            // bt_echo_on
            // 
            this.bt_echo_on.Location = new System.Drawing.Point(280, 545);
            this.bt_echo_on.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_echo_on.Name = "bt_echo_on";
            this.bt_echo_on.Size = new System.Drawing.Size(101, 30);
            this.bt_echo_on.TabIndex = 43;
            this.bt_echo_on.Text = "ECHO ON";
            this.bt_echo_on.UseVisualStyleBackColor = true;
            this.bt_echo_on.Click += new System.EventHandler(this.bt_echo_on_Click);
            // 
            // bt_echo_off
            // 
            this.bt_echo_off.Location = new System.Drawing.Point(280, 594);
            this.bt_echo_off.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_echo_off.Name = "bt_echo_off";
            this.bt_echo_off.Size = new System.Drawing.Size(101, 30);
            this.bt_echo_off.TabIndex = 44;
            this.bt_echo_off.Text = "ECHO OFF";
            this.bt_echo_off.UseVisualStyleBackColor = true;
            this.bt_echo_off.Click += new System.EventHandler(this.bt_echo_off_Click);
            // 
            // bt_park
            // 
            this.bt_park.Location = new System.Drawing.Point(619, 481);
            this.bt_park.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_park.Name = "bt_park";
            this.bt_park.Size = new System.Drawing.Size(75, 30);
            this.bt_park.TabIndex = 45;
            this.bt_park.Text = "PARK";
            this.bt_park.UseVisualStyleBackColor = true;
            this.bt_park.Click += new System.EventHandler(this.bt_park_Click);
            // 
            // bt_set_park
            // 
            this.bt_set_park.Location = new System.Drawing.Point(368, 481);
            this.bt_set_park.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_set_park.Name = "bt_set_park";
            this.bt_set_park.Size = new System.Drawing.Size(104, 30);
            this.bt_set_park.TabIndex = 46;
            this.bt_set_park.Text = "SET PARK";
            this.bt_set_park.UseVisualStyleBackColor = true;
            this.bt_set_park.Click += new System.EventHandler(this.bt_set_park_Click);
            // 
            // upDown_sync_az
            // 
            this.upDown_sync_az.Location = new System.Drawing.Point(97, 303);
            this.upDown_sync_az.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.upDown_sync_az.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.upDown_sync_az.Name = "upDown_sync_az";
            this.upDown_sync_az.Size = new System.Drawing.Size(63, 22);
            this.upDown_sync_az.TabIndex = 47;
            // 
            // upDown_sync_alt
            // 
            this.upDown_sync_alt.Location = new System.Drawing.Point(332, 303);
            this.upDown_sync_alt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.upDown_sync_alt.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.upDown_sync_alt.Name = "upDown_sync_alt";
            this.upDown_sync_alt.Size = new System.Drawing.Size(61, 22);
            this.upDown_sync_alt.TabIndex = 48;
            // 
            // upDown_sync_shutter
            // 
            this.upDown_sync_shutter.Location = new System.Drawing.Point(587, 303);
            this.upDown_sync_shutter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.upDown_sync_shutter.Name = "upDown_sync_shutter";
            this.upDown_sync_shutter.Size = new System.Drawing.Size(64, 22);
            this.upDown_sync_shutter.TabIndex = 49;
            this.upDown_sync_shutter.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // bt_stop_com
            // 
            this.bt_stop_com.Location = new System.Drawing.Point(400, 545);
            this.bt_stop_com.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_stop_com.Name = "bt_stop_com";
            this.bt_stop_com.Size = new System.Drawing.Size(104, 30);
            this.bt_stop_com.TabIndex = 50;
            this.bt_stop_com.Text = "STOP COM";
            this.bt_stop_com.UseVisualStyleBackColor = true;
            this.bt_stop_com.Click += new System.EventHandler(this.bt_stop_com_Click);
            // 
            // bt_stop_dome
            // 
            this.bt_stop_dome.Location = new System.Drawing.Point(153, 481);
            this.bt_stop_dome.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_stop_dome.Name = "bt_stop_dome";
            this.bt_stop_dome.Size = new System.Drawing.Size(107, 30);
            this.bt_stop_dome.TabIndex = 51;
            this.bt_stop_dome.Text = "STOP DOME";
            this.bt_stop_dome.UseVisualStyleBackColor = true;
            this.bt_stop_dome.Click += new System.EventHandler(this.bt_stop_dome_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(101, 534);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(148, 103);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 52;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(549, 540);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(204, 85);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 53;
            this.pictureBox2.TabStop = false;
            // 
            // bt_init_com
            // 
            this.bt_init_com.Location = new System.Drawing.Point(400, 594);
            this.bt_init_com.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_init_com.Name = "bt_init_com";
            this.bt_init_com.Size = new System.Drawing.Size(104, 30);
            this.bt_init_com.TabIndex = 54;
            this.bt_init_com.Text = "INIT COM";
            this.bt_init_com.UseVisualStyleBackColor = true;
            this.bt_init_com.Click += new System.EventHandler(this.bt_init_com_Click);
            // 
            // bt_tracking_on
            // 
            this.bt_tracking_on.Location = new System.Drawing.Point(797, 454);
            this.bt_tracking_on.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_tracking_on.Name = "bt_tracking_on";
            this.bt_tracking_on.Size = new System.Drawing.Size(140, 27);
            this.bt_tracking_on.TabIndex = 55;
            this.bt_tracking_on.Text = "TRACKING ON";
            this.bt_tracking_on.UseVisualStyleBackColor = true;
            this.bt_tracking_on.Click += new System.EventHandler(this.bt_tracking_Click);
            // 
            // bt_home_full
            // 
            this.bt_home_full.Location = new System.Drawing.Point(796, 246);
            this.bt_home_full.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_home_full.Name = "bt_home_full";
            this.bt_home_full.Size = new System.Drawing.Size(141, 27);
            this.bt_home_full.TabIndex = 56;
            this.bt_home_full.Text = "FULL HOME";
            this.bt_home_full.UseVisualStyleBackColor = true;
            this.bt_home_full.Click += new System.EventHandler(this.bt_home_full_Click);
            // 
            // bt_display
            // 
            this.bt_display.Location = new System.Drawing.Point(796, 353);
            this.bt_display.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_display.Name = "bt_display";
            this.bt_display.Size = new System.Drawing.Size(141, 27);
            this.bt_display.TabIndex = 57;
            this.bt_display.Text = "DISPLAY";
            this.bt_display.UseVisualStyleBackColor = true;
            this.bt_display.Click += new System.EventHandler(this.bt_display_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(12, 11);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(55, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 58;
            this.pictureBox3.TabStop = false;
            // 
            // date_picker
            // 
            this.date_picker.Location = new System.Drawing.Point(100, 172);
            this.date_picker.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.date_picker.Name = "date_picker";
            this.date_picker.Size = new System.Drawing.Size(283, 22);
            this.date_picker.TabIndex = 59;
            // 
            // bt_config
            // 
            this.bt_config.Location = new System.Drawing.Point(796, 298);
            this.bt_config.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_config.Name = "bt_config";
            this.bt_config.Size = new System.Drawing.Size(141, 28);
            this.bt_config.TabIndex = 60;
            this.bt_config.Text = "CONFIG";
            this.bt_config.UseVisualStyleBackColor = true;
            this.bt_config.Click += new System.EventHandler(this.bt_config_Click);
            // 
            // bt_motor_activation
            // 
            this.bt_motor_activation.Location = new System.Drawing.Point(783, 190);
            this.bt_motor_activation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_motor_activation.Name = "bt_motor_activation";
            this.bt_motor_activation.Size = new System.Drawing.Size(176, 34);
            this.bt_motor_activation.TabIndex = 61;
            this.bt_motor_activation.Text = "MOTOR ACTIVATION";
            this.bt_motor_activation.UseVisualStyleBackColor = true;
            this.bt_motor_activation.Click += new System.EventHandler(this.bt__force_open_Click);
            // 
            // textBox_debug
            // 
            this.textBox_debug.HideSelection = false;
            this.textBox_debug.Location = new System.Drawing.Point(995, 303);
            this.textBox_debug.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox_debug.Multiline = true;
            this.textBox_debug.Name = "textBox_debug";
            this.textBox_debug.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_debug.Size = new System.Drawing.Size(177, 338);
            this.textBox_debug.TabIndex = 62;
            this.textBox_debug.TextChanged += new System.EventHandler(this.textBox_debug_TextChanged);
            // 
            // bt_tracking_off
            // 
            this.bt_tracking_off.Location = new System.Drawing.Point(796, 402);
            this.bt_tracking_off.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_tracking_off.Name = "bt_tracking_off";
            this.bt_tracking_off.Size = new System.Drawing.Size(141, 28);
            this.bt_tracking_off.TabIndex = 63;
            this.bt_tracking_off.Text = "TRACKING OFF";
            this.bt_tracking_off.UseVisualStyleBackColor = true;
            this.bt_tracking_off.Click += new System.EventHandler(this.bt_tracking_off_Click);
            // 
            // bt_weather_path
            // 
            this.bt_weather_path.Location = new System.Drawing.Point(1011, 190);
            this.bt_weather_path.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_weather_path.Name = "bt_weather_path";
            this.bt_weather_path.Size = new System.Drawing.Size(140, 34);
            this.bt_weather_path.TabIndex = 64;
            this.bt_weather_path.Text = "WEATHER PATH";
            this.bt_weather_path.UseVisualStyleBackColor = true;
            this.bt_weather_path.Click += new System.EventHandler(this.bt_weather_path_Click);
            // 
            // bt_config_path
            // 
            this.bt_config_path.Location = new System.Drawing.Point(1011, 245);
            this.bt_config_path.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_config_path.Name = "bt_config_path";
            this.bt_config_path.Size = new System.Drawing.Size(140, 33);
            this.bt_config_path.TabIndex = 65;
            this.bt_config_path.Text = "CONFIG PATH";
            this.bt_config_path.UseVisualStyleBackColor = true;
            this.bt_config_path.Click += new System.EventHandler(this.bt_config_path_Click);
            // 
            // label_weather_conditions
            // 
            this.label_weather_conditions.AutoSize = true;
            this.label_weather_conditions.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label_weather_conditions.Location = new System.Drawing.Point(355, 74);
            this.label_weather_conditions.Name = "label_weather_conditions";
            this.label_weather_conditions.Size = new System.Drawing.Size(213, 17);
            this.label_weather_conditions.TabIndex = 66;
            this.label_weather_conditions.Text = "GOOD WEATHER CONDITIONS";
            // 
            // bt_stall_flag
            // 
            this.bt_stall_flag.Location = new System.Drawing.Point(797, 603);
            this.bt_stall_flag.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_stall_flag.Name = "bt_stall_flag";
            this.bt_stall_flag.Size = new System.Drawing.Size(140, 30);
            this.bt_stall_flag.TabIndex = 67;
            this.bt_stall_flag.Text = "STALL FLAG";
            this.bt_stall_flag.UseVisualStyleBackColor = true;
            this.bt_stall_flag.Click += new System.EventHandler(this.bt_stall_flag_Click);
            // 
            // label_rain
            // 
            this.label_rain.AutoSize = true;
            this.label_rain.Location = new System.Drawing.Point(67, 50);
            this.label_rain.Name = "label_rain";
            this.label_rain.Size = new System.Drawing.Size(32, 17);
            this.label_rain.TabIndex = 68;
            this.label_rain.Text = "rain";
            // 
            // label_humidity
            // 
            this.label_humidity.AutoSize = true;
            this.label_humidity.Location = new System.Drawing.Point(53, 82);
            this.label_humidity.Name = "label_humidity";
            this.label_humidity.Size = new System.Drawing.Size(60, 17);
            this.label_humidity.TabIndex = 69;
            this.label_humidity.Text = "humidity";
            // 
            // label_cloud
            // 
            this.label_cloud.AutoSize = true;
            this.label_cloud.Location = new System.Drawing.Point(61, 16);
            this.label_cloud.Name = "label_cloud";
            this.label_cloud.Size = new System.Drawing.Size(42, 17);
            this.label_cloud.TabIndex = 70;
            this.label_cloud.Text = "cloud";
            // 
            // label_wind
            // 
            this.label_wind.AutoSize = true;
            this.label_wind.Location = new System.Drawing.Point(55, 116);
            this.label_wind.Name = "label_wind";
            this.label_wind.Size = new System.Drawing.Size(36, 17);
            this.label_wind.TabIndex = 71;
            this.label_wind.Text = "wind";
            // 
            // label_az_telescope
            // 
            this.label_az_telescope.AutoSize = true;
            this.label_az_telescope.Location = new System.Drawing.Point(38, 8);
            this.label_az_telescope.Name = "label_az_telescope";
            this.label_az_telescope.Size = new System.Drawing.Size(91, 17);
            this.label_az_telescope.TabIndex = 72;
            this.label_az_telescope.Text = "AZ telescope";
            // 
            // label_alt_telescope
            // 
            this.label_alt_telescope.AutoSize = true;
            this.label_alt_telescope.Location = new System.Drawing.Point(37, 43);
            this.label_alt_telescope.Name = "label_alt_telescope";
            this.label_alt_telescope.Size = new System.Drawing.Size(99, 17);
            this.label_alt_telescope.TabIndex = 73;
            this.label_alt_telescope.Text = "ALT telescope";
            // 
            // label_temperature_in
            // 
            this.label_temperature_in.AutoSize = true;
            this.label_temperature_in.Location = new System.Drawing.Point(35, 22);
            this.label_temperature_in.Name = "label_temperature_in";
            this.label_temperature_in.Size = new System.Drawing.Size(100, 17);
            this.label_temperature_in.TabIndex = 74;
            this.label_temperature_in.Text = "temperature in";
            // 
            // label_dew_point_in
            // 
            this.label_dew_point_in.AutoSize = true;
            this.label_dew_point_in.Location = new System.Drawing.Point(43, 60);
            this.label_dew_point_in.Name = "label_dew_point_in";
            this.label_dew_point_in.Size = new System.Drawing.Size(83, 17);
            this.label_dew_point_in.TabIndex = 75;
            this.label_dew_point_in.Text = "dew point in";
            // 
            // label_humidity_in
            // 
            this.label_humidity_in.AutoSize = true;
            this.label_humidity_in.Location = new System.Drawing.Point(47, 97);
            this.label_humidity_in.Name = "label_humidity_in";
            this.label_humidity_in.Size = new System.Drawing.Size(72, 17);
            this.label_humidity_in.TabIndex = 76;
            this.label_humidity_in.Text = "humidty in";
            // 
            // label_temperature_out
            // 
            this.label_temperature_out.AutoSize = true;
            this.label_temperature_out.Location = new System.Drawing.Point(32, 134);
            this.label_temperature_out.Name = "label_temperature_out";
            this.label_temperature_out.Size = new System.Drawing.Size(109, 17);
            this.label_temperature_out.TabIndex = 77;
            this.label_temperature_out.Text = "temperature out";
            // 
            // label_dew_point_out
            // 
            this.label_dew_point_out.AutoSize = true;
            this.label_dew_point_out.Location = new System.Drawing.Point(40, 171);
            this.label_dew_point_out.Name = "label_dew_point_out";
            this.label_dew_point_out.Size = new System.Drawing.Size(92, 17);
            this.label_dew_point_out.TabIndex = 78;
            this.label_dew_point_out.Text = "dew point out";
            // 
            // label_humidity_out
            // 
            this.label_humidity_out.AutoSize = true;
            this.label_humidity_out.Location = new System.Drawing.Point(44, 208);
            this.label_humidity_out.Name = "label_humidity_out";
            this.label_humidity_out.Size = new System.Drawing.Size(84, 17);
            this.label_humidity_out.TabIndex = 79;
            this.label_humidity_out.Text = "humidity out";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(1208, 167);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Window;
            this.splitContainer1.Panel1.Controls.Add(this.label_wind_threshold);
            this.splitContainer1.Panel1.Controls.Add(this.label_humidity_threshold);
            this.splitContainer1.Panel1.Controls.Add(this.label_color_rain);
            this.splitContainer1.Panel1.Controls.Add(this.label_color_cloud);
            this.splitContainer1.Panel1.Controls.Add(this.label_cloud);
            this.splitContainer1.Panel1.Controls.Add(this.label_rain);
            this.splitContainer1.Panel1.Controls.Add(this.label_humidity);
            this.splitContainer1.Panel1.Controls.Add(this.label_wind);
            this.splitContainer1.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Window;
            this.splitContainer1.Panel2.Controls.Add(this.label_temperature_in);
            this.splitContainer1.Panel2.Controls.Add(this.label_humidity_out);
            this.splitContainer1.Panel2.Controls.Add(this.label_dew_point_in);
            this.splitContainer1.Panel2.Controls.Add(this.label_dew_point_out);
            this.splitContainer1.Panel2.Controls.Add(this.label_humidity_in);
            this.splitContainer1.Panel2.Controls.Add(this.label_temperature_out);
            this.splitContainer1.Size = new System.Drawing.Size(203, 474);
            this.splitContainer1.SplitterDistance = 221;
            this.splitContainer1.TabIndex = 80;
            // 
            // label_color_rain
            // 
            this.label_color_rain.AutoSize = true;
            this.label_color_rain.BackColor = System.Drawing.SystemColors.MenuText;
            this.label_color_rain.Location = new System.Drawing.Point(105, 50);
            this.label_color_rain.Name = "label_color_rain";
            this.label_color_rain.Size = new System.Drawing.Size(24, 17);
            this.label_color_rain.TabIndex = 81;
            this.label_color_rain.Text = "    ";
            // 
            // label_color_cloud
            // 
            this.label_color_cloud.AutoSize = true;
            this.label_color_cloud.BackColor = System.Drawing.SystemColors.MenuText;
            this.label_color_cloud.Location = new System.Drawing.Point(109, 16);
            this.label_color_cloud.Name = "label_color_cloud";
            this.label_color_cloud.Size = new System.Drawing.Size(24, 17);
            this.label_color_cloud.TabIndex = 81;
            this.label_color_cloud.Text = "    ";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Location = new System.Drawing.Point(1208, 93);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.SystemColors.Window;
            this.splitContainer2.Panel1.Controls.Add(this.label_az_telescope);
            this.splitContainer2.Panel1.Controls.Add(this.label_alt_telescope);
            this.splitContainer2.Size = new System.Drawing.Size(203, 123);
            this.splitContainer2.SplitterDistance = 70;
            this.splitContainer2.TabIndex = 82;
            // 
            // label_humidity_threshold
            // 
            this.label_humidity_threshold.AutoSize = true;
            this.label_humidity_threshold.Location = new System.Drawing.Point(14, 151);
            this.label_humidity_threshold.Name = "label_humidity_threshold";
            this.label_humidity_threshold.Size = new System.Drawing.Size(123, 17);
            this.label_humidity_threshold.TabIndex = 83;
            this.label_humidity_threshold.Text = "humidity threshold";
            // 
            // label_wind_threshold
            // 
            this.label_wind_threshold.AutoSize = true;
            this.label_wind_threshold.Location = new System.Drawing.Point(22, 186);
            this.label_wind_threshold.Name = "label_wind_threshold";
            this.label_wind_threshold.Size = new System.Drawing.Size(99, 17);
            this.label_wind_threshold.TabIndex = 84;
            this.label_wind_threshold.Text = "wind threshold";
            // 
            // UNIDomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(1457, 729);
            this.Controls.Add(this.bt_stall_flag);
            this.Controls.Add(this.label_weather_conditions);
            this.Controls.Add(this.bt_config_path);
            this.Controls.Add(this.bt_weather_path);
            this.Controls.Add(this.bt_tracking_off);
            this.Controls.Add(this.textBox_debug);
            this.Controls.Add(this.bt_motor_activation);
            this.Controls.Add(this.bt_config);
            this.Controls.Add(this.date_picker);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.bt_display);
            this.Controls.Add(this.bt_home_full);
            this.Controls.Add(this.bt_tracking_on);
            this.Controls.Add(this.bt_init_com);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.bt_stop_dome);
            this.Controls.Add(this.bt_stop_com);
            this.Controls.Add(this.upDown_sync_shutter);
            this.Controls.Add(this.upDown_sync_alt);
            this.Controls.Add(this.upDown_sync_az);
            this.Controls.Add(this.bt_set_park);
            this.Controls.Add(this.bt_park);
            this.Controls.Add(this.bt_echo_off);
            this.Controls.Add(this.bt_echo_on);
            this.Controls.Add(this.bt_sync_shutter);
            this.Controls.Add(this.upDown_shutter);
            this.Controls.Add(this.bt_close_shutter);
            this.Controls.Add(this.bt_open_shutter);
            this.Controls.Add(this.bt_west_relat_alt);
            this.Controls.Add(this.bt_east_relat_alt);
            this.Controls.Add(this.bt_west_relat_az);
            this.Controls.Add(this.bt_east_relat_az);
            this.Controls.Add(this.bt_west_abs_alt);
            this.Controls.Add(this.bt_east_abs_alt);
            this.Controls.Add(this.bt_east_abs_az);
            this.Controls.Add(this.label_relat_upDown);
            this.Controls.Add(this.label_abs_upDown);
            this.Controls.Add(this.upDown_alt_relat);
            this.Controls.Add(this.upDown_az_relat);
            this.Controls.Add(this.label_stall_shutter);
            this.Controls.Add(this.label_stall_alt);
            this.Controls.Add(this.label_stall_az);
            this.Controls.Add(this.dateTime);
            this.Controls.Add(this.bt_back_up);
            this.Controls.Add(this.bt_log);
            this.Controls.Add(this.bt_sync_alt);
            this.Controls.Add(this.bt_sync_az);
            this.Controls.Add(this.upDown_alt_abs);
            this.Controls.Add(this.upDown_az_abs);
            this.Controls.Add(this.label_cod_shutter);
            this.Controls.Add(this.label_cod_alt);
            this.Controls.Add(this.label_cod_az);
            this.Controls.Add(this.label_shutter);
            this.Controls.Add(this.label_alt);
            this.Controls.Add(this.label_az);
            this.Controls.Add(this.bt_home_shutter);
            this.Controls.Add(this.bt_home_alt);
            this.Controls.Add(this.bt_home_az);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.splitContainer2);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "UNIDomeForm";
            this.Text = "UNIDome Driver";
            this.Load += new System.EventHandler(this.UNIDomeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.upDown_az_abs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_alt_abs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_az_relat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_alt_relat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_shutter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_sync_az)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_sync_alt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_sync_shutter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_home_az;
        private System.Windows.Forms.Button bt_home_alt;
        private System.Windows.Forms.Button bt_home_shutter;
        private System.Windows.Forms.Label label_az;
        private System.Windows.Forms.Label label_alt;
        private System.Windows.Forms.Label label_cod_az;
        private System.Windows.Forms.Label label_cod_alt;
        private System.Windows.Forms.Label label_cod_shutter;
        private System.Windows.Forms.NumericUpDown upDown_az_abs;
        private System.Windows.Forms.NumericUpDown upDown_alt_abs;
        private System.Windows.Forms.Button bt_sync_az;
        private System.Windows.Forms.Button bt_sync_alt;
        private System.Windows.Forms.Button bt_log;
        private System.Windows.Forms.Button bt_back_up;
        private System.Windows.Forms.DateTimePicker dateTime;
        private System.Windows.Forms.Label label_stall_az;
        private System.Windows.Forms.Label label_stall_alt;
        private System.Windows.Forms.Label label_stall_shutter;
        private System.Windows.Forms.NumericUpDown upDown_az_relat;
        private System.Windows.Forms.NumericUpDown upDown_alt_relat;
        private System.Windows.Forms.Label label_abs_upDown;
        private System.Windows.Forms.Label label_relat_upDown;
        private System.Windows.Forms.Button bt_east_abs_az;
        private System.Windows.Forms.Button bt_east_abs_alt;
        private System.Windows.Forms.Button bt_west_abs_alt;
        private System.Windows.Forms.Button bt_east_relat_az;
        private System.Windows.Forms.Button bt_west_relat_az;
        private System.Windows.Forms.Button bt_east_relat_alt;
        private System.Windows.Forms.Button bt_west_relat_alt;
        private System.Windows.Forms.Button bt_open_shutter;
        private System.Windows.Forms.Button bt_close_shutter;
        private System.Windows.Forms.NumericUpDown upDown_shutter;
        private System.Windows.Forms.Button bt_sync_shutter;
        private System.Windows.Forms.Label label_shutter;
        private System.Windows.Forms.Button bt_echo_on;
        private System.Windows.Forms.Button bt_echo_off;
        private System.Windows.Forms.Button bt_park;
        private System.Windows.Forms.Button bt_set_park;
        private System.Windows.Forms.NumericUpDown upDown_sync_az;
        private System.Windows.Forms.NumericUpDown upDown_sync_alt;
        private System.Windows.Forms.NumericUpDown upDown_sync_shutter;
        private System.Windows.Forms.Button bt_stop_com;
        private System.Windows.Forms.Button bt_stop_dome;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button bt_init_com;
        private System.Windows.Forms.Button bt_tracking_on;
        private System.Windows.Forms.Button bt_home_full;
        private System.Windows.Forms.Button bt_display;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.DateTimePicker date_picker;
        private System.Windows.Forms.Button bt_config;
        private System.Windows.Forms.Button bt_motor_activation;
        private System.Windows.Forms.TextBox textBox_debug;
        private System.Windows.Forms.Button bt_tracking_off;
        private System.Windows.Forms.OpenFileDialog openFileDialog_weather;
        private System.Windows.Forms.Button bt_weather_path;
        private System.Windows.Forms.Button bt_config_path;
        private System.Windows.Forms.OpenFileDialog openFileDialog_config;
        private System.Windows.Forms.Label label_weather_conditions;
        private System.Windows.Forms.Button bt_stall_flag;
        private System.Windows.Forms.Label label_rain;
        private System.Windows.Forms.Label label_humidity;
        private System.Windows.Forms.Label label_cloud;
        private System.Windows.Forms.Label label_wind;
        private System.Windows.Forms.Label label_az_telescope;
        private System.Windows.Forms.Label label_alt_telescope;
        private System.Windows.Forms.Label label_temperature_in;
        private System.Windows.Forms.Label label_dew_point_in;
        private System.Windows.Forms.Label label_humidity_in;
        private System.Windows.Forms.Label label_temperature_out;
        private System.Windows.Forms.Label label_dew_point_out;
        private System.Windows.Forms.Label label_humidity_out;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label_color_rain;
        private System.Windows.Forms.Label label_color_cloud;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label label_humidity_threshold;
        private System.Windows.Forms.Label label_wind_threshold;
    }
}
