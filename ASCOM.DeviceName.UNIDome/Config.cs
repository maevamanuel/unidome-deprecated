using ASCOM;
using ASCOM.Astrometry;
using ASCOM.Astrometry.AstroUtils;
using ASCOM.DeviceInterface;
using ASCOM.Utilities;

using System;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;

namespace ASCOM.UNIDome
{
    /// <summary>
    /// Configuration class of the driver
    /// </summary>
    /// <remarks>
    /// Add state machine flags, getters and setters
    /// </remarks>
    public class Config
    {
        /// <value>_IsConnected by default to false</value>
        private bool _IsConnected = false;

        /// <value>_IsSlewing by default to false</value>
        private bool _IsSlewing = false;

        /// <value>_AtHome by default to false</value>
        private bool _AtHome = false;

        /// <value>_AzimuthDest by default to 0</value>
        private double _AzimuthDest = 0;

        /// <value>_AltitudeDest by default to 0</value>
        private double _AltitudeDest = 0;

        /// <value>_ShutterStatus by default to closed</value>
        private ShutterState _ShutterStatus = ShutterState.shutterClosed;

        /// <value>_Shuttervalue is the aperture between 0 and 100%</value>
        private double _Shuttervalue = 0;

        /// <summary>
        /// Accessor and mutator of IsConnected member
        /// </summary>
        public bool IsConnected
        {
            get { return this._IsConnected; }
            set { this._IsConnected = value; }
        }

        /// <summary>
        /// Accessor and mutator of IsSlewing member
        /// </summary>
        public bool IsSlewing
        {
            get { return this._IsSlewing; }
            set { this._IsSlewing = value; }
        }

        /// <summary>
        /// Accessor and mutator of AtHome member
        /// </summary>
        public bool AtHome
        {
            get { return this._AtHome; }
            set { this._AtHome = value; }
        }

        /// <summary>
        /// Accessor and mutator of AzimuthDest member
        /// </summary>
        public double AzimuthDest
        {
            get { return this._AzimuthDest; }
            set { this._AzimuthDest = value; }
        }

        /// <summary>
        /// Accessor and mutator of AltitudeDest member
        /// </summary>
        public double AltitudeDest
        {
            get { return this._AltitudeDest; }
            set { this._AltitudeDest = value; }
        }

        /// <summary>
        /// Accessor and mutator of ShutterStatus object
        /// </summary>
        public ShutterState ShutterStatus
        {
            get { return this._ShutterStatus; }
            set { this._ShutterStatus = value; }
        }

        /// <summary>
        /// Accessor and mutator of ShutterValue object
        /// </summary>
        public double ShutterValue
        {
            get { return this._Shuttervalue; }
            set { this._Shuttervalue = value; }
        }
    }
}
