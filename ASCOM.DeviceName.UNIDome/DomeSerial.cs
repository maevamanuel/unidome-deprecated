using ASCOM;
using ASCOM.Astrometry;
using ASCOM.Astrometry.AstroUtils;
using ASCOM.DeviceInterface;
using ASCOM.Utilities;

using System;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;

namespace ASCOM.UNIDome
{
    /// <summary>
    /// DomeSerial class of the driver
    /// </summary>
    /// <remarks>
    /// Define architecture of commands and intialize 2 serial communications
    /// </remarks>
    class DomeSerial
    {
        /// <value>_domeSerial by default to null</value>
        public SerialPort _domeSerial = null;
        /// <value>_altSerial by default to null</value>
        public SerialPort _altSerial = null;

        /// <value>home_az command</value>
        public string home_az = "AHM " + "1";
        /// <value>home_alt command</value>
        public string home_alt = "OHM " + "1";
        /// <value>home_shutter command</value>
        public string home_shutter = "SHM " + "1";

        /// <value>OpenShutter command</value>
        public string OpenShutter = "SMA ";
        /// <value>CloseShutter command</value>
        public string CloseShutter = "SMA ";
        /// <value>move_abs_alt command</value>
        public string move_abs_alt = "OMA ";
        /// <value>move_abs_az command</value>
        public string move_abs_az = "AMA ";

        /// <value>move_relat_alt command</value>
        public string move_relat_alt = "OMR ";
        /// <value>move_relat_az command</value>
        public string move_relat_az = "AMR ";

        /// <value>SyncToAzimuth command</value>
        public string SyncToAzimuth = "AP ";
        /// <value>SyncToAltitude command</value>
        public string SyncToAltitude = "OP ";
        /// <value>SyncToShutter command</value>
        public string SyncToShutter = "SP ";

        /// <value>stall_flag_az command</value>
        public string stall_flag_az = "APR ST";
        /// <value>stall_flag_alt command</value>
        public string stall_flag_alt = "OPR ST";
        /// <value>stall_flag_shutter command</value>
        public string stall_flag_shutter = "SPR ST";

        /// <value>stop_az command</value>
        public string stop_az = "ASL 0";
        /// <value>stop_alt command</value>
        public string stop_alt = "OSL 0";
        /// <value>stop_shutter command</value>
        public string stop_shutter = "SSL 0";

        /// <value>position_flag_az command</value>
        public string position_flag_az = "APR P";
        /// <value>position_flag_alt command</value>
        public string position_flag_alt = "OPR P";
        /// <value>position_flag_shutter command</value>
        public string position_flag_shutter = "SPR P";

        /// <value>echo_mode_off command</value>
        public string echo_mode_off = "AEM " + "1";
        /// <value>echo_mode_on command</value>
        public string echo_mode_on = "AEM " + "0";

        /// <value>mv_flag_az command, 0 not moving, 1 moving</value>
        public string mv_flag_az = "APR MV";
        /// <value>mv_flag_alt command, 0 not moving, 1 moving</value>
        public string mv_flag_alt = "OPR MV";
        /// <value>mv_flag_shutter command, 0 not moving, 1 moving</value>
        public string mv_flag_shutter = "SPR MV";

        /// <value>all_flag_az command</value>
        public string all_flag_az = "APR AL";
        /// <value>all_flag_alt command</value>
        public string all_flag_alt = "OPR AL";
        /// <value>all_flag_shutter command</value>
        public string all_flag_shutter = "SPR AL";

        /// <value>velocity_az command</value>
        public string velocity_az = "AVM ";
        /// <value>velocity_alt command</value>
        public string velocity_alt = "OVM ";

        /// <summary>
        /// Constructor of the DomeSerial class
        /// Initialize a serial communication for the dome
        /// </summary>
        public DomeSerial(string comPortDome)
        {
            _domeSerial = new SerialPort();
            _domeSerial.PortName = comPortDome;
            _domeSerial.BaudRate = 9600;
            _domeSerial.Parity = Parity.None;
            _domeSerial.DataBits = 8;
            _domeSerial.StopBits = StopBits.One;
            _domeSerial.Handshake = Handshake.None;
            _domeSerial.DtrEnable = true;
            _domeSerial.Open();
        }

        /// <summary>
        /// Initialize a separated serial communication for the alt
        /// </summary>
        /// <param name="comPortalt">COM string</param>
        public void altSerial(string comPortalt)
        {
            _altSerial = new SerialPort();
            _altSerial.PortName = comPortalt;
            _altSerial.BaudRate = 115200;
            _altSerial.Parity = Parity.None;
            _altSerial.DataBits = 8;
            _altSerial.StopBits = StopBits.One;
            _altSerial.Handshake = Handshake.None;
            _altSerial.Open();
        }

        /// <summary>
        /// This function sends command via the dome Serial communication
        /// and wait for response from the dome until NewLine is sent
        /// </summary>
        /// <remarks>
        /// Command and NewLine MUST be sent separately.
        /// DiscardInBuffer MUST be used before ReadLine function.
        /// </remarks>
        /// <param name="command">Command string</param>
        public string SendCommand(string command)
        {
            string retval;
            _domeSerial.DiscardOutBuffer();
            _domeSerial.WriteLine(Environment.NewLine);
            _domeSerial.WriteLine(command);
            _domeSerial.WriteLine(Environment.NewLine);
            _domeSerial.DiscardInBuffer();
            retval = null;
            retval = _domeSerial.ReadLine();
            return retval;
        }

        /// <summary>
        /// This function sends command via the dome Serial communication
        /// and wait for response from the dome until NewLine is sent
        /// </summary>
        /// /// <remarks>
        /// Command and NewLine MUST be sent separately.
        /// DiscardInBuffer MUST be used before ReadLine function.
        /// </remarks>
        /// <param name="command">Command string</param>
        /// <param name="value">Value string</param>
        public string SendCommand(string command, string value)
        {
            string retval;
            _domeSerial.DiscardOutBuffer();
            _domeSerial.WriteLine(Environment.NewLine);
            _domeSerial.WriteLine(command + value);
            _domeSerial.WriteLine(Environment.NewLine);
            _domeSerial.DiscardInBuffer();
            retval = null;
            retval = _domeSerial.ReadLine();
            return retval;
        }
    }
}
