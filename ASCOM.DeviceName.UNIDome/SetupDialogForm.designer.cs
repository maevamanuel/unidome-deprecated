
namespace ASCOM.UNIDome
{
    partial class SetupDialogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.cmdOK = new System.Windows.Forms.Button();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.picASCOM = new System.Windows.Forms.PictureBox();
			this.label2 = new System.Windows.Forms.Label();
			this.labelalt = new System.Windows.Forms.Label();
			this.chkTrace = new System.Windows.Forms.CheckBox();
			this.comboBoxComPort = new System.Windows.Forms.ComboBox();
			this.comboBoxComaltPort = new System.Windows.Forms.ComboBox();
			this.label_enable_weather = new System.Windows.Forms.Label();
			this.checkBox_weather = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.picASCOM)).BeginInit();
			this.SuspendLayout();
			// 
			// cmdOK
			// 
			this.cmdOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.cmdOK.Location = new System.Drawing.Point(281, 112);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(59, 24);
			this.cmdOK.TabIndex = 0;
			this.cmdOK.Text = "OK";
			this.cmdOK.UseVisualStyleBackColor = true;
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cmdCancel.Location = new System.Drawing.Point(281, 142);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(59, 25);
			this.cmdCancel.TabIndex = 1;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.UseVisualStyleBackColor = true;
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(123, 31);
			this.label1.TabIndex = 2;
			this.label1.Text = "Construct your driver\'s setup dialog here.";
			// 
			// picASCOM
			// 
			this.picASCOM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.picASCOM.Cursor = System.Windows.Forms.Cursors.Hand;
			this.picASCOM.Image = global::ASCOM.UNIDome.Properties.Resources.ASCOM;
			this.picASCOM.Location = new System.Drawing.Point(292, 9);
			this.picASCOM.Name = "picASCOM";
			this.picASCOM.Size = new System.Drawing.Size(48, 56);
			this.picASCOM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.picASCOM.TabIndex = 3;
			this.picASCOM.TabStop = false;
			this.picASCOM.Click += new System.EventHandler(this.BrowseToAscom);
			this.picASCOM.DoubleClick += new System.EventHandler(this.BrowseToAscom);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(17, 90);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(62, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "COM Dome";
			// 
			// labelalt
			// 
			this.labelalt.AutoSize = true;
			this.labelalt.Location = new System.Drawing.Point(22, 120);
			this.labelalt.Name = "labelalt";
			this.labelalt.Size = new System.Drawing.Size(46, 13);
			this.labelalt.TabIndex = 6;
			this.labelalt.Text = "COM Alt";
			// 
			// chkTrace
			// 
			this.chkTrace.AutoSize = true;
			this.chkTrace.Location = new System.Drawing.Point(77, 148);
			this.chkTrace.Name = "chkTrace";
			this.chkTrace.Size = new System.Drawing.Size(69, 17);
			this.chkTrace.TabIndex = 7;
			this.chkTrace.Text = "Trace on";
			this.chkTrace.UseVisualStyleBackColor = true;
			// 
			// comboBoxComPort
			// 
			this.comboBoxComPort.FormattingEnabled = true;
			this.comboBoxComPort.Location = new System.Drawing.Point(113, 87);
			this.comboBoxComPort.Name = "comboBoxComPort";
			this.comboBoxComPort.Size = new System.Drawing.Size(90, 21);
			this.comboBoxComPort.TabIndex = 8;
			// 
			// comboBoxComaltPort
			// 
			this.comboBoxComaltPort.FormattingEnabled = true;
			this.comboBoxComaltPort.Location = new System.Drawing.Point(113, 118);
			this.comboBoxComaltPort.Name = "comboBoxComaltPort";
			this.comboBoxComaltPort.Size = new System.Drawing.Size(90, 21);
			this.comboBoxComaltPort.TabIndex = 9;
			// 
			// label_enable_weather
			// 
			this.label_enable_weather.AutoSize = true;
			this.label_enable_weather.Location = new System.Drawing.Point(17, 56);
			this.label_enable_weather.Name = "label_enable_weather";
			this.label_enable_weather.Size = new System.Drawing.Size(131, 13);
			this.label_enable_weather.TabIndex = 10;
			this.label_enable_weather.Text = "Enable weather protection";
			// 
			// checkBox_weather
			// 
			this.checkBox_weather.AutoSize = true;
			this.checkBox_weather.Checked = true;
			this.checkBox_weather.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox_weather.Location = new System.Drawing.Point(169, 55);
			this.checkBox_weather.Name = "checkBox_weather";
			this.checkBox_weather.Size = new System.Drawing.Size(15, 14);
			this.checkBox_weather.TabIndex = 11;
			this.checkBox_weather.UseVisualStyleBackColor = true;
			// 
			// SetupDialogForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(350, 175);
			this.Controls.Add(this.checkBox_weather);
			this.Controls.Add(this.label_enable_weather);
			this.Controls.Add(this.comboBoxComPort);
			this.Controls.Add(this.comboBoxComaltPort);
			this.Controls.Add(this.chkTrace);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.labelalt);
			this.Controls.Add(this.picASCOM);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cmdCancel);
			this.Controls.Add(this.cmdOK);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SetupDialogForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "UNIDome Setup";
			((System.ComponentModel.ISupportInitialize)(this.picASCOM)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picASCOM;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelalt;
        private System.Windows.Forms.CheckBox chkTrace;
        private System.Windows.Forms.ComboBox comboBoxComPort;
        private System.Windows.Forms.ComboBox comboBoxComaltPort;
		private System.Windows.Forms.Label label_enable_weather;
		private System.Windows.Forms.CheckBox checkBox_weather;
	}
}